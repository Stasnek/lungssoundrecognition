package com.nekrasov.diser.lungssoundrecognition.soundprocess.audio;

public enum SpectrumType {
    SPECTRUM_AVG_FILTERED, SPECTRUM_AVG, SAMPLES_AVG_FILTERED, SAMPLES_AVG, SPECTRAL_FLUX_AVG_FILTERED, SPECTRAL_FLUX_AVG
}
