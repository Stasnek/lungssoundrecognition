package com.nekrasov.diser.lungssoundrecognition.soundprocess.dataset.impl;


import com.nekrasov.diser.lungssoundrecognition.soundprocess.audio.AudioDataExtractor;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.audio.SpectrumType;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.csv.CSVHandler;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.dataset.DataSetProvider;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.lungssoundclass.AudioFileSpectrum;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.lungssoundclass.LungsSoundClass;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.utils.NetworkUtils;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public abstract class MultiClassDataSetProvider<T> implements DataSetProvider {
    private static final File CSV_ROOT = new File("src/main/resources/csv");
    protected int windowSize;
    private int fftSample;
    private List<LungsSoundClass> classes;
    private SpectrumType spectrumType;
    private final CSVHandler csvHandler = new CSVHandler();
    private File spectrumDataFile;
    private List<Integer> testIndexes;

    public MultiClassDataSetProvider(List<LungsSoundClass> classes, int fftSample, int windowSize,
                                     SpectrumType spectrumType, List<Integer> testIndexes) throws IOException {
        this.classes = classes;
        this.fftSample = fftSample;
        this.windowSize = windowSize;
        this.spectrumType = spectrumType;
        this.testIndexes = testIndexes;
        spectrumDataFile = new File(CSV_ROOT, spectrumType.toString() + "_" + fftSample + ".csv");
        calculateSpectrum();
    }

    public abstract T createTestDataSet();

    public abstract T createDataSet();

    public List<LungsSoundClass> getLungsSoundClasses() {
        return classes;
    }

    protected void calculateSpectrum() throws IOException {
        if (spectrumDataFile.exists()) {
            System.out.println("Read data from file: " + spectrumDataFile.getPath());
            parseSpectrumFromCSV(spectrumDataFile);
        } else {
            System.out.println("Data not found.");
            System.out.println("Calculating spectrum...");
            for (LungsSoundClass soundClass : getClasses()) {
                calculateSpectrum(soundClass);
            }
            dumpSpectrum();
        }

        for (LungsSoundClass soundClass : getClasses()) {
            fillTrainAndTestDataSets(soundClass);
        }
    }


    private void calculateSpectrum(LungsSoundClass soundClass) {
        FilenameFilter filenameFilter = (dir, name) -> (name.toLowerCase().endsWith(".wav")
                || name.toLowerCase().endsWith(".mp3"));
        for (File file : soundClass.getTrainSourcesRoot().listFiles(filenameFilter)) {
            try {
                double[] spectrum = getSpectrum(file);
                double[] normalizedSpectrum = NetworkUtils.getNormalizedDataUpperLowBorder(spectrum);
                soundClass.addAudioFileSpectrum(new AudioFileSpectrum(file, normalizedSpectrum));
            } catch (Exception e) {
                e.printStackTrace();
                System.err.println("at " + file.getAbsolutePath());
            }
        }
    }


    private void parseSpectrumFromCSV(File spectrumDataFile) {
        try {
            Map<String, List<AudioFileSpectrum>> spectrumData = csvHandler.readCSVFile(spectrumDataFile);
            for (LungsSoundClass soundClass : classes) {
                List<AudioFileSpectrum> audioFilesSpectrum = spectrumData.get(soundClass.getClassName());
                soundClass.getAudioFilesSpectrum().addAll(audioFilesSpectrum);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void dumpSpectrum() throws IOException {
        if (!spectrumDataFile.createNewFile()) {
            throw new IOException("Can not create " + spectrumDataFile.getPath());
        }
        csvHandler.printCsv(classes, spectrumDataFile);
    }


    private double[] getSpectrum(File file) throws Exception {
        switch (spectrumType) {
            case SPECTRUM_AVG_FILTERED:
                return toDoubleArray(AudioDataExtractor.getAvgSpectrumFiltered(file, fftSample,
                        windowSize));
            case SPECTRUM_AVG:
                return toDoubleArray(AudioDataExtractor.getAvgSpectrum(file, fftSample,
                        windowSize));
            case SAMPLES_AVG_FILTERED:
                return toDoubleArray(AudioDataExtractor.getAvgSamplesFiltered(file, fftSample,
                        windowSize));
            case SAMPLES_AVG:
                return toDoubleArray(AudioDataExtractor.getAvgSamples(file, fftSample,
                        windowSize));
            case SPECTRAL_FLUX_AVG_FILTERED:
                return toDoubleArray(AudioDataExtractor.getRectifiedSpectralFluxAvgFiltered(file, fftSample,
                        windowSize));
            case SPECTRAL_FLUX_AVG:
                return toDoubleArray(AudioDataExtractor.getRectifiedSpectralFluxAvg(file, fftSample,
                        windowSize));
            default:
                return toDoubleArray(AudioDataExtractor.getAvgSpectrumFiltered(file, fftSample,
                        windowSize));
        }
    }


    private void fillTrainAndTestDataSets(LungsSoundClass soundClass) {
        List<AudioFileSpectrum> audioFilesSpectrum = soundClass.getAudioFilesSpectrum();
        for (int i = 0; i < audioFilesSpectrum.size(); i++) {
            if (testIndexes.contains(i)) {
                soundClass.addRowToTestSpectum(audioFilesSpectrum.get(i));
            } else {
                soundClass.addRowToTrainSpectrum(audioFilesSpectrum.get(i));
            }
        }
    }


    private double[] toDoubleArray(float[] floatArray) {
        double[] doubleArray = new double[floatArray.length];
        for (int i = 0; i < floatArray.length; i++) {
            doubleArray[i] = floatArray[i];
        }
        return doubleArray;
    }


    public List<LungsSoundClass> getClasses() {
        return classes;
    }

    public void setClasses(List<LungsSoundClass> classes) {
        this.classes = classes;
    }

    public SpectrumType getSpectrumType() {
        return spectrumType;
    }

    public void setSpectrumType(SpectrumType spectrumType) {
        this.spectrumType = spectrumType;
    }
}
