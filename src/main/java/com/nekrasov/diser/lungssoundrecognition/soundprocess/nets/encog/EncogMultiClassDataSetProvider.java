package com.nekrasov.diser.lungssoundrecognition.soundprocess.nets.encog;

import com.nekrasov.diser.lungssoundrecognition.soundprocess.audio.SpectrumType;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.dataset.impl.MultiClassDataSetProvider;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.lungssoundclass.AudioFileSpectrum;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.lungssoundclass.LungsSoundClass;
import org.encog.ml.data.MLData;
import org.encog.ml.data.MLDataPair;
import org.encog.ml.data.basic.BasicMLData;
import org.encog.ml.data.basic.BasicMLDataPair;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class EncogMultiClassDataSetProvider extends MultiClassDataSetProvider<DataSet> {

    public EncogMultiClassDataSetProvider(List<LungsSoundClass> classes, int fftSample, int windowSize,
                                          SpectrumType spectrumType, List<Integer> testIndexes) throws IOException {
        super(classes, fftSample, windowSize, spectrumType, testIndexes);
    }


    public DataSet createTestDataSet() {
        DataSet dataSetTest = new DataSet();
        for (LungsSoundClass soundClass : getClasses()) {
            fillDataSet(dataSetTest, soundClass.getTestSpectrum(), soundClass.getOutput(), windowSize,
                    "---- " + soundClass.getClassName() + " ----");
        }
        System.out.println("test dataset size: " + dataSetTest.getDataSet().size());
        return dataSetTest;
    }


    public DataSet createDataSet() {
        DataSet dataSetTrain = new DataSet();
        for (LungsSoundClass soundClass : getClasses()) {
            fillDataSet(dataSetTrain, soundClass.getTrainSpectrum(), soundClass.getOutput(), windowSize,
                    "---- " + soundClass.getClassName() + " ----");
        }
        System.out.println("train dataset size: " + dataSetTrain.getDataSet().size());
        return dataSetTrain;
    }


    private void fillDataSet(DataSet dataSet, List<AudioFileSpectrum> audioFilesSpectrum,
                             double[] output, int input, String label) {
        for (AudioFileSpectrum fileSpectrum : audioFilesSpectrum) {
            MLData inputData = new BasicMLData(Arrays.copyOf(fileSpectrum.getSpectrum(), input));
            MLData outputData = new BasicMLData(output);

            MLDataPair row = new BasicMLDataPair(inputData, outputData);
            dataSet.addLabel(label + " File " + fileSpectrum.getFile().getName());
            dataSet.add(row);
        }
    }
}
