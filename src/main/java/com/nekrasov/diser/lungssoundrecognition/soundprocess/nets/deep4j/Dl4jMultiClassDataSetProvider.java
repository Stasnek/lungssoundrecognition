package com.nekrasov.diser.lungssoundrecognition.soundprocess.nets.deep4j;

import com.nekrasov.diser.lungssoundrecognition.soundprocess.audio.SpectrumType;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.dataset.impl.MultiClassDataSetProvider;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.lungssoundclass.AudioFileSpectrum;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.lungssoundclass.LungsSoundClass;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.factory.Nd4j;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Dl4jMultiClassDataSetProvider extends MultiClassDataSetProvider<DataSet> {

    public Dl4jMultiClassDataSetProvider(List<LungsSoundClass> list, int fftSample, int windowSize,
                                         SpectrumType spectrumType, List<Integer> testIndexes) throws IOException {
        super(list, fftSample, windowSize, spectrumType, testIndexes);
    }


    @Override
    public DataSet createTestDataSet() {
        double[][] inputs = new double[getInputTestsAmount()][windowSize];
        double[][] outputs = new double[getInputTestsAmount()][getClasses().size()];
        List<String> labelNames = new ArrayList<>();

        int index = 0;
        for (LungsSoundClass soundClass : getClasses()) {
            List<AudioFileSpectrum> testSpectrums = soundClass.getTestSpectrum();
            for (int i = 0; i < testSpectrums.size(); i++) {
                AudioFileSpectrum audioFileSpectrum = testSpectrums.get(i);
                double[] spectrum = audioFileSpectrum.getSpectrum();
                System.arraycopy(spectrum, 0, inputs[index + i], 0, spectrum.length);
                double[] output = soundClass.getOutput();
                System.arraycopy(output, 0, outputs[index + i], 0, output.length);
                labelNames.add(audioFileSpectrum.getFile().getName());
            }
            index += testSpectrums.size();
        }
        INDArray inputNdArray = Nd4j.create(inputs);
        INDArray outputNdArray = Nd4j.create(outputs);
        return new DataSet(inputNdArray, outputNdArray);
    }

    @Override
    public DataSet createDataSet() {
        int inputTrainsAmount = getInputTrainsAmount();
        double[][] inputs = new double[inputTrainsAmount][windowSize];
        double[][] outputs = new double[inputTrainsAmount][getClasses().size()];
        List<String> labelNames = new ArrayList<>();
        int index = 0;
        for (LungsSoundClass soundClass : getClasses()) {
            List<AudioFileSpectrum> trainSpectrums = soundClass.getTrainSpectrum();
            for (int i = 0; i < trainSpectrums.size(); i++) {
                AudioFileSpectrum audioFileSpectrum = trainSpectrums.get(i);
                double[] spectrum = audioFileSpectrum.getSpectrum();
                System.arraycopy(spectrum, 0, inputs[index + i], 0, spectrum.length);
                double[] output = soundClass.getOutput();
                System.arraycopy(output, 0, outputs[index + i], 0, output.length);
                labelNames.add(audioFileSpectrum.getFile().getName());
            }
            index += trainSpectrums.size();
        }
        INDArray inputNdArray = Nd4j.create(inputs);
        INDArray outputNdArray = Nd4j.create(outputs);
        return new DataSet(inputNdArray, outputNdArray);
    }


    private int getInputTrainsAmount() {
        int amount = 0;
        for (LungsSoundClass soundClass : getClasses()) {
            amount += soundClass.getTrainSpectrum().size();
        }
        return amount;
    }


    private int getInputTestsAmount() {
        int amount = 0;
        for (LungsSoundClass soundClass : getClasses()) {
            amount += soundClass.getTestSpectrum().size();
        }
        return amount;
    }
}
