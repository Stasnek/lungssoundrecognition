package com.nekrasov.diser.lungssoundrecognition.soundprocess.utils;

import org.neuroph.core.Layer;
import org.neuroph.core.NeuralNetwork;
import org.neuroph.core.Neuron;
import org.neuroph.core.transfer.TransferFunction;

import java.util.Arrays;
import java.util.List;

/**
 *
 */
public final class NetworkUtils {
    private NetworkUtils() {
    }

    public static void setTransferFunction(NeuralNetwork network, TransferFunction function) {
        int levelCount = 0;
        for (Object layer : network.getLayers()) {
            for (Neuron neuron : ((Layer) layer).getNeurons()) {
                if (levelCount != 0) {
                    neuron.setTransferFunction(function);
                }
            }
            levelCount++;
        }
    }


    public static double[] getNormalizedDataUpperLowBorder(double[] inputData) {
        double[] outputData = new double[inputData.length];
        double min = Arrays.stream(inputData).min().getAsDouble();
        double max = Arrays.stream(inputData).max().getAsDouble();
        for (int i = 0; i < inputData.length; i++) {
            outputData[i] = ((inputData[i] - min) / (max - min)) * 0.99 + 0.01;
        }
//        Arrays.stream(inputData).forEach(i -> i = (i - min) / (max - min));
        return outputData;
    }


    public static double[] getNormalizedData(double[] inputData) {
        double[] outputData = new double[inputData.length];
        double min = Arrays.stream(inputData).min().getAsDouble();
        double max = Arrays.stream(inputData).max().getAsDouble();
        for (int i = 0; i < inputData.length; i++) {
            outputData[i] = (inputData[i] - min) / (max - min);
        }
        return outputData;
    }

    public static float[] getNormalizedData(float[] inputData) {
        float[] outputData = new float[inputData.length];
        float min = getMinValue(inputData);
        float max = getMaxValue(inputData);
        for (int i = 0; i < inputData.length; i++) {
            outputData[i] = (inputData[i] - min) / (max - min);
        }
        return outputData;
    }


    private static float getMaxValue(float[] data) {
        float max = Float.MIN_VALUE;
        for (float aData : data) {
            if (aData > max) {
                max = aData;
            }
        }
        return max;
    }


    private static float getMinValue(float[] data) {
        float min = Float.MAX_VALUE;
        for (float aData : data) {
            if (aData < min) {
                min = aData;
            }
        }
        return min;
    }


    public static void printLearningRuleName(NeuralNetwork network) {
        System.out.println("Learning rule: " + network.getLearningRule().getClass().getSimpleName());
    }

    public static void printLayers(NeuralNetwork network) {
        int levelCount = 0;
        for (Layer layer : (List<Layer>) network.getLayers()) {
            System.out.println(levelCount + " " + layer.getClass().getSimpleName() + " level");
            System.out.println("neurons: " + layer.getNeurons().size());
            levelCount++;
        }

    }
}
