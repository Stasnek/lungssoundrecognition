package com.nekrasov.diser.lungssoundrecognition.soundprocess.nets.deep4j;

import com.nekrasov.diser.lungssoundrecognition.soundprocess.dataset.DataSetProvider;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.nets.NeuralNet;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.report.NetworkReport;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import java.util.Iterator;


public class Deep4jMLP implements NeuralNet {
    private static final int NUMBER_OF_EPOCHS = 5000;
    private final DataSetProvider dataSetProvider;
    private final String name;
    private final MultiLayerNetwork network;
    private final int inputSize;
    private final int outputSize;
    private double learningRate;
    private int numberOfEpochs = NUMBER_OF_EPOCHS;
    private NetworkReport networkReport;

    public Deep4jMLP(DataSetProvider dataSetProvider, String name, int inputSize, int outputSize) {
        this.dataSetProvider = dataSetProvider;
        this.name = name;
        this.inputSize = inputSize;
        this.outputSize = outputSize;
        network = buildNetwork();
        networkReport = new NetworkReport();
    }

    private MultiLayerNetwork buildNetwork() {
        int seed = 123;

        int numHiddenNodes = inputSize;

        //log.info("Build model....");
        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                .seed(seed)
                .updater(new Nesterovs(learningRate, 0.9))
                .list()
                .layer(0, new DenseLayer.Builder().nIn(inputSize).nOut(numHiddenNodes)
                        .weightInit(WeightInit.XAVIER)
                        .activation(Activation.SIGMOID)
                        .build())
                .layer(1, new DenseLayer.Builder().nIn(numHiddenNodes).nOut(numHiddenNodes / 2)
                        .weightInit(WeightInit.XAVIER)
                        .activation(Activation.SIGMOID)
                        .build())
                .layer(2, new DenseLayer.Builder().nIn(numHiddenNodes / 2).nOut(numHiddenNodes / 4)
                        .weightInit(WeightInit.XAVIER)
                        .activation(Activation.SIGMOID)
                        .build())
                .layer(3, new DenseLayer.Builder().nIn(numHiddenNodes / 4).nOut(numHiddenNodes / 8)
                        .weightInit(WeightInit.XAVIER)
                        .activation(Activation.SIGMOID)
                        .build())
                .layer(4, new DenseLayer.Builder().nIn(numHiddenNodes / 8).nOut(10)
                        .weightInit(WeightInit.XAVIER)
                        .activation(Activation.SIGMOID)
                        .build())
                .layer(5, new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
                        .weightInit(WeightInit.XAVIER)
                        .activation(Activation.SIGMOID)
                        .nIn(10).nOut(outputSize).build())
                .pretrain(false).backprop(true).build();


        MultiLayerNetwork network = new MultiLayerNetwork(conf);
        network.init();
        network.setListeners(new ScoreIterationListener(100));
        return network;
    }

    @Override
    public void train() {
        DataSet trainDataSet = (DataSet) dataSetProvider.createDataSet();
        networkReport.notchStartTime();
        for (int n = 0; n < numberOfEpochs; n++) {
            network.fit(trainDataSet);
        }
        networkReport.notchEndTime("Training");
        test(trainDataSet);
    }

    public void test(DataSet dataSet) {
        // test the neural network
        System.out.println("Evaluate model....");
        Evaluation eval = new Evaluation(outputSize);
        Iterator<DataSet> iterator = dataSet.iterator();
        while (iterator.hasNext()) {
            DataSet d = iterator.next();
            INDArray predicted = network.output(d.getFeatureMatrix());
            INDArray labels = d.getLabels();
            networkReport.notchStartTime();
            eval.eval(labels, predicted);
        }

        networkReport.notchEndTime("Testing");

        //Print the evaluation statistics
        System.out.println(eval.stats());
    }

    @Override
    public void test() {
        networkReport = new NetworkReport();
        DataSet testDataSet = (DataSet) dataSetProvider.createTestDataSet();
        test(testDataSet);
    }

    @Override
    public void setLearningRate(double learningRate) {
        network.setLearningRate(learningRate);
        this.learningRate = learningRate;
    }

    @Override
    public void setBatchMode(boolean batchMode) {

    }

    @Override
    public void setMaxError(double maxError) {

    }

    @Override
    public void setMaxIterations(int maxIterations) {
        network.setEpochCount(maxIterations);
        numberOfEpochs = maxIterations;
    }

    @Override
    public double getTrainingTime() {
        return 0;
    }
}
