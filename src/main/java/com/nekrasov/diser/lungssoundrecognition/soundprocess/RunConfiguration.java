package com.nekrasov.diser.lungssoundrecognition.soundprocess;

import com.nekrasov.diser.lungssoundrecognition.soundprocess.audio.SpectrumType;

public class RunConfiguration {
    private int fftSample;
    private int windowSize;
    private double learningRate;
    private double maxError;
    private int maxIterations;
    private boolean batchMode;
    private SpectrumType spectrumType;

    public RunConfiguration(int fftSample, int windowSize, double learningRate, double maxError,
                            int maxIterations, boolean batchMode, SpectrumType spectrumType) {
        this.fftSample = fftSample;
        this.windowSize = windowSize;
        this.learningRate = learningRate;
        this.maxError = maxError;
        this.maxIterations = maxIterations;
        this.batchMode = batchMode;
        this.spectrumType = spectrumType;
    }


    public int getFftSample() {
        return fftSample;
    }

    public void setFftSample(int fftSample) {
        this.fftSample = fftSample;
    }

    public int getWindowSize() {
        return windowSize;
    }

    public void setWindowSize(int windowSize) {
        this.windowSize = windowSize;
    }

    public double getLearningRate() {
        return learningRate;
    }

    public void setLearningRate(double learningRate) {
        this.learningRate = learningRate;
    }

    public double getMaxError() {
        return maxError;
    }

    public void setMaxError(double maxError) {
        this.maxError = maxError;
    }

    public int getMaxIterations() {
        return maxIterations;
    }

    public void setMaxIterations(int maxIterations) {
        this.maxIterations = maxIterations;
    }

    public boolean isBatchMode() {
        return batchMode;
    }

    public void setBatchMode(boolean batchMode) {
        this.batchMode = batchMode;
    }

    public SpectrumType getSpectrumType() {
        return spectrumType;
    }

    public void setSpectrumType(SpectrumType spectrumType) {
        this.spectrumType = spectrumType;
    }
}
