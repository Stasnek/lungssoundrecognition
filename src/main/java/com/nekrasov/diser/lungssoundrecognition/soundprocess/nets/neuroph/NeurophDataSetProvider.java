package com.nekrasov.diser.lungssoundrecognition.soundprocess.nets.neuroph;


import com.nekrasov.diser.lungssoundrecognition.soundprocess.dataset.impl.DataSetProviderImpl;
import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;

import java.util.Arrays;
import java.util.Map;

public class NeurophDataSetProvider extends DataSetProviderImpl<DataSet> {

    private DataSet dataSet;
    private int windowSize;

    public NeurophDataSetProvider(int fftSample, int windowSize, boolean useFilteredRecords) {
        super(fftSample, windowSize, useFilteredRecords);
        this.windowSize = windowSize;
    }


    public void clearData() {
        if (dataSet != null) {
            dataSet.clear();
        }
        sickSpectrum.clear();
        unSickSpectrum.clear();
    }

    public DataSet createTestDataSet() {
        clearData();
        calculateSpectrumTest();
        fillDataSetRows(true);
        return dataSet;
    }

    public DataSet createDataSet() {
        clearData();
        calculateSpectrumTrain();
        fillDataSetRows(false);
        return dataSet;
    }


    protected void fillDataSetRows(boolean isTest) {
        dataSet = new DataSet(windowSize, 2);
        addRowsToDataSet(sickSpectrum, windowSize, SICK_OUTPUT, UNSICK_OUTPUT, "---- sick! ----");
        addRowsToDataSet(unSickSpectrum, windowSize, UNSICK_OUTPUT, SICK_OUTPUT, "---- not sick! ----");
    }

    protected void addRowsToDataSet(Map<String, double[]> spectrumMap, int input, double output1,
                                    double output2, String label) {
        for (Map.Entry<String, double[]> spectrum : spectrumMap.entrySet()) {
            DataSetRow row = new DataSetRow(Arrays.copyOf(spectrum.getValue(), input), new double[]{output1, output2});
            row.setLabel(label + " File " + spectrum.getKey());
            dataSet.addRow(row);
        }
    }

    public DataSet getDataSet() {
        return dataSet;
    }
}
