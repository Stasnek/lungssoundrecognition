package com.nekrasov.diser.lungssoundrecognition.soundprocess.lungssoundclass;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public final class NetworkClassBuilder {
    private static final String DATASET_PATH = "audio/multi-class-dataset/";
    private static final File CRACKLES = new File(DATASET_PATH + "crackles");
    private static final File RHONCHI = new File(DATASET_PATH + "rhonchi");
    private static final File RUB = new File(DATASET_PATH + "rub");
    private static final File STRIDOR = new File(DATASET_PATH + "stridor");
    private static final File WHEEZES = new File(DATASET_PATH + "wheezes");
    private static final File NORMAL = new File(DATASET_PATH + "normal");
    private static final double ONE = 0.99;
    private static final double ZERO = 0.01;
    private static List<LungsSoundClass> lungsSoundClasses;


    private NetworkClassBuilder() {
    }

    private static final int CLASS_AMOUNT = 3;

    public static List<LungsSoundClass> build() {
        lungsSoundClasses = new ArrayList<>();
        lungsSoundClasses.add(new LungsSoundClass("crackles", CRACKLES, CRACKLES, buildOutput()));
//        lungsSoundClasses.add(new LungsSoundClass("rhonchi", RHONCHI, RHONCHI, buildOutput()));
//        lungsSoundClasses.add(new LungsSoundClass("rub", RUB, RUB, buildOutput()));
//        lungsSoundClasses.add(new LungsSoundClass("stridor", STRIDOR, STRIDOR, buildOutput()));
        lungsSoundClasses.add(new LungsSoundClass("wheezes", WHEEZES, WHEEZES, buildOutput()));
        lungsSoundClasses.add(new LungsSoundClass("normal", NORMAL, NORMAL, buildOutput()));
        return lungsSoundClasses;
    }

    private static double[] buildOutput() {
        double[] output = new double[CLASS_AMOUNT];
        int indexOfClass = lungsSoundClasses.size();
        for (int i = 0; i < output.length; i++) {
            output[i] = ZERO;
        }
        output[indexOfClass] = ONE;
        return output;
    }
}
