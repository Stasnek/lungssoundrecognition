package com.nekrasov.diser.lungssoundrecognition.soundprocess.report;

public class NetworkItemResult {
    private double[] output;
    private double[] ideal;
    private String label;
    private boolean failed;

    public NetworkItemResult(double[] output, double[] ideal, String label) {
        this.output = output;
        this.ideal = ideal;
        this.label = label;
    }

    public double[] getOutput() {
        return output;
    }

    public void setOutput(double[] output) {
        this.output = output;
    }

    public double[] getIdeal() {
        return ideal;
    }

    public void setIdeal(double[] ideal) {
        this.ideal = ideal;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isFailed() {
        return failed;
    }

    public void setFailed(boolean failed) {
        this.failed = failed;
    }
}
