package com.nekrasov.diser.lungssoundrecognition.soundprocess.nets.neuroph;

import com.nekrasov.diser.lungssoundrecognition.soundprocess.dataset.DataSetProvider;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.nets.NeuralNet;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.utils.NetworkUtils;
import org.neuroph.core.NeuralNetwork;
import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;
import org.neuroph.core.learning.LearningRule;
import org.neuroph.core.transfer.Sigmoid;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.nnet.learning.BackPropagation;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class NeurophMLP implements NeuralNet {
    private MultiLayerPerceptron mlp;
    private DataSet trainingSet;
    private String name;
    private DataSetProvider<DataSet> dataSetProvider;
    private LearningRule learningRule;
    private double trainingTime;


    public NeurophMLP(DataSetProvider dataSetProvider, String name, Integer... hiddenLayers) {
        this.dataSetProvider = dataSetProvider;
        this.name = name;
        trainingSet = this.dataSetProvider.createDataSet();
        learningRule = new BackPropagation();
        learningRule.addListener(new BackPropagationListener());


        List<Integer> layers = new ArrayList<Integer>();
        layers.add(trainingSet.getInputSize());
        layers.addAll(Arrays.asList(hiddenLayers));
        layers.add(trainingSet.getOutputSize());
        mlp = new MultiLayerPerceptron(layers);
        mlp.setLearningRule((BackPropagation) learningRule);

        mlp.randomizeWeights(0, 0.5);

//        System.out.println("initial weights values");
//        System.out.println(Arrays.toString(mlp.getWeights()));
        System.out.println("InputNeurons size: " + mlp.getInputNeurons().size());
        System.out.println("OutputNeurons size: " + mlp.getOutputNeurons().size());
    }


    public void test(DataSet testSet) {
        NeuralNetwork loadedMlPerceptron = NeuralNetwork.createFromFile(name + ".nnet");
        DecimalFormat df = new DecimalFormat("#.###########");

        for (DataSetRow testSetRow : testSet.getRows()) {
            loadedMlPerceptron.setInput(testSetRow.getInput());
            loadedMlPerceptron.calculate();
            System.out.print(" Output: " + df.format(loadedMlPerceptron.getOutput()[0]));
            String rowLabel = testSetRow.getLabel();
            if (validateRow(loadedMlPerceptron, rowLabel)) {
                System.out.println(" " + rowLabel + "     ------  FAILED!");
            } else {
                System.out.println(" " + rowLabel);
            }
        }
    }

    private boolean validateRow(NeuralNetwork loadedMlPerceptron, String rowLabel) {
        return rowLabel.contains("---- sick! ----") && loadedMlPerceptron.getOutput()[0] < 0.4
                || rowLabel.contains("---- not sick! ----") && loadedMlPerceptron.getOutput()[0] > 0.4;
    }

    public void test() {
        DataSet testSet = dataSetProvider.createTestDataSet();
        test(testSet);
    }


    /**
     * Runs this sample
     */
    public void train() {
        NetworkUtils.setTransferFunction(mlp, new Sigmoid());
        // learn the training set
        System.out.println("Training neural network...");
        double startTime = System.currentTimeMillis();
        mlp.learn(trainingSet);
        trainingTime = (System.currentTimeMillis() - startTime) / 1000;

        // save trained neural network
        mlp.save(name + ".nnet");

        // test perceptron
        System.out.println("Testing trained neural network");
        test(trainingSet);

        System.out.println("Training time is " + trainingTime + " s.");
    }

    public LearningRule getLearningRule() {
        return learningRule;
    }

    public void setLearningRule(LearningRule learningRule) {
        this.learningRule = learningRule;
    }

    public void setLearningRate(double learningRate) {
        ((BackPropagation) learningRule).setLearningRate(learningRate);
    }

    public void setBatchMode(boolean batchMode) {
        ((BackPropagation) learningRule).setBatchMode(batchMode);
    }

    public void setMaxError(double maxError) {
        ((BackPropagation) learningRule).setMaxError(maxError);
    }

    public void setMaxIterations(int maxIterations) {
        ((BackPropagation) learningRule).setMaxIterations(maxIterations);
    }

    public double getTrainingTime() {
        return trainingTime;
    }
}
