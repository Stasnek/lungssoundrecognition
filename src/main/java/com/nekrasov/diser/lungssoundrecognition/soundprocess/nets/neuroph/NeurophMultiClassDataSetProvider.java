package com.nekrasov.diser.lungssoundrecognition.soundprocess.nets.neuroph;

import com.nekrasov.diser.lungssoundrecognition.soundprocess.audio.SpectrumType;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.dataset.impl.MultiClassDataSetProvider;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.lungssoundclass.AudioFileSpectrum;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.lungssoundclass.LungsSoundClass;
import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class NeurophMultiClassDataSetProvider extends MultiClassDataSetProvider<DataSet> {

    public NeurophMultiClassDataSetProvider(List<LungsSoundClass> classes, int fftSample, int windowSize,
                                            SpectrumType spectrumType, List<Integer> testIndexes) throws IOException {
        super(classes, fftSample, windowSize, spectrumType, testIndexes);
    }

    public DataSet createTestDataSet() {
        DataSet dataSetTest = new DataSet(windowSize, getClasses().size());
        for (LungsSoundClass soundClass : getClasses()) {
            fillDataSet(dataSetTest, soundClass.getTestSpectrum(), soundClass.getOutput(), windowSize,
                    "---- " + soundClass.getClassName() + " ----");
        }
        return dataSetTest;
    }

    public DataSet createDataSet() {
        DataSet dataSetTrain = new DataSet(windowSize, getClasses().size());
        for (LungsSoundClass soundClass : getClasses()) {
            fillDataSet(dataSetTrain, soundClass.getTrainSpectrum(), soundClass.getOutput(), windowSize,
                    "---- " + soundClass.getClassName() + " ----");
        }
        return dataSetTrain;
    }


    private void fillDataSet(DataSet dataSet, List<AudioFileSpectrum> audioFilesSpectrum,
                             double[] output, int input, String label) {
        for (AudioFileSpectrum fileSpectrum : audioFilesSpectrum) {
            DataSetRow row = new DataSetRow(Arrays.copyOf(fileSpectrum.getSpectrum(), input), output);
            row.setLabel(label + " File " + fileSpectrum.getFile().getName());
            dataSet.addRow(row);
        }
    }
}
