package com.nekrasov.diser.lungssoundrecognition.soundprocess.audio.plot;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import java.awt.Color;
import java.io.File;
import java.util.Map;

public class PlotUtils {

    public static void buildPlotImage(File srcAudioFile, float[] plotData, String folderName) throws Exception {
        String name = srcAudioFile.getName();
        XYDataset dataset = createDataset(plotData, name);
        JFreeChart lineChartObject = createChart(dataset, name);
        int width = 2048;    /* Width of the image */
        int height = 768;   /* Height of the image */


        File saveFolder = new File("target/" + folderName);
        if (!saveFolder.exists() && !saveFolder.mkdirs()) {
            throw new Exception("cant create " + saveFolder);
        }

        File lineChart = new File(saveFolder, name.substring(0, name.length() - 4) + ".png");
        ChartUtilities.saveChartAsPNG(lineChart, lineChartObject, width, height);
    }

    public static void buildPlotImage(File srcAudioFile, Map<String, float[]> plotData, String folderName) throws Exception {
        String name = srcAudioFile.getName();
        XYDataset dataset = createDataset(plotData);
        JFreeChart lineChartObject = createChart(dataset, name);
        int width = 2048;    /* Width of the image */
        int height = 768;   /* Height of the image */


        File saveFolder = new File("target/" + folderName);
        if (!saveFolder.exists() && !saveFolder.mkdirs()) {
            throw new Exception("cant create " + saveFolder);
        }

        File lineChart = new File(saveFolder, name.substring(0, name.length() - 4) + ".png");
        ChartUtilities.saveChartAsPNG(lineChart, lineChartObject, width, height);
    }


    public static void buildCombinedPlotImage(Map<String, float[]> plotData, String folderName, String plotName) throws Exception {
        XYDataset dataset = createDatasetCombined(plotData);
        JFreeChart lineChartObject = createChart(dataset, plotName);
        int width = 1920;    /* Width of the image */
        int height = 2000;   /* Height of the image */


        File saveFolder = new File("target/" + folderName);
        if (!saveFolder.exists() && !saveFolder.mkdirs()) {
            throw new Exception("cant create " + saveFolder);
        }

        File lineChart = new File(saveFolder, plotName + ".png");
        ChartUtilities.saveChartAsPNG(lineChart, lineChartObject, width, height);
    }

    private static void fillSeries(XYSeries series, float[] data, float offset) {
        for (int i = 0; i < data.length; i++) {
            series.add(i, data[i] + offset);
        }
    }

    private static void fillSeries(XYSeries series, float[] data) {
        for (int i = 0; i < data.length; i++) {
            series.add(i, data[i]);
        }
    }

    private static XYDataset createDatasetCombined(Map<String, float[]> plotData) {
        final XYSeriesCollection dataset = new XYSeriesCollection();
        float count = 0;
        for (Map.Entry<String, float[]> entry : plotData.entrySet()) {
            XYSeries series = new XYSeries(entry.getKey());
            float[] data = entry.getValue();
            fillSeries(series, data, count);
            dataset.addSeries(series);
            count++;
        }
        return dataset;
    }

    private static float getMaxValue(float[] data) {
        float max = Float.MIN_VALUE;
        for (float aData : data) {
            if (aData > max) {
                max = aData;
            }
        }
        return max;
    }

    private static XYDataset createDataset(Map<String, float[]> plotData) {
        final XYSeriesCollection dataset = new XYSeriesCollection();

        for (Map.Entry<String, float[]> entry : plotData.entrySet()) {
            XYSeries series = new XYSeries(entry.getKey());
            fillSeries(series, entry.getValue());
            dataset.addSeries(series);
        }

        return dataset;
    }


    private static XYDataset createDataset(float[] plotData, String name) {
        final XYSeriesCollection dataset = new XYSeriesCollection();
        XYSeries series = new XYSeries(name);
        fillSeries(series, plotData);
        dataset.addSeries(series);
        return dataset;
    }


    private static JFreeChart createChart(final XYDataset dataset, String name) {

        // create the chart...
        final JFreeChart chart = ChartFactory.createXYLineChart(
                name,      // chart title
                "X",                      // x axis label
                "Y",                      // y axis label
                dataset,                  // data
                PlotOrientation.VERTICAL,
                true,                     // include legend
                true,                     // tooltips
                false                     // urls
        );

        // NOW DO SOME OPTIONAL CUSTOMISATION OF THE CHART...
        chart.setBackgroundPaint(Color.white);

        // get a reference to the plot for further customisation...
        final XYPlot plot = chart.getXYPlot();
        plot.setBackgroundPaint(Color.lightGray);
        plot.setDomainGridlinePaint(Color.white);
        plot.setRangeGridlinePaint(Color.white);

        final XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        plot.setRenderer(renderer);

        // change the auto tick unit selection to integer units only...
        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        // OPTIONAL CUSTOMISATION COMPLETED.

        return chart;
    }
}
