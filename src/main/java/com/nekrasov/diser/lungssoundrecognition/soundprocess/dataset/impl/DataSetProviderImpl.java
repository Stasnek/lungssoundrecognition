package com.nekrasov.diser.lungssoundrecognition.soundprocess.dataset.impl;

import com.nekrasov.diser.lungssoundrecognition.soundprocess.audio.AudioDataExtractor;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.dataset.DataSetProvider;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.lungssoundclass.LungsSoundClass;

import java.io.File;
import java.io.FilenameFilter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
public abstract class DataSetProviderImpl<T> implements DataSetProvider {
    protected static final File SICK_FOLDER = new File("src/main/resources/wav/sick");
    protected static final File UNSICK_FOLDER = new File("src/main/resources/wav/unsick");
    protected static final File TEST_SICK_DATA_FOLDER = new File("src/main/resources/wav/testing-sick-data");
    protected static final File TEST_UNSICK_DATA_FOLDER = new File("src/main/resources/wav/testing-unsick-data");

    public static final double SICK_OUTPUT = 0.99;
    public static final double UNSICK_OUTPUT = 0.01;
    protected static final String DATASET_PATH = "src/main/resources/wav/dataset/";
    protected static final String DATASET_1_KHZ_PATH = DATASET_PATH + "filtered-to-1kgz/";
    protected static final File SICK_FOLDER_3MEGYPT = new File(DATASET_PATH + "3megypt/mp3/sick");
    protected static final File UNSICK_FOLDER_3MEGYPT = new File(DATASET_PATH + "3megypt/mp3/unsick");
    protected static final File SICK_FOLDER_PULMONARY = new File(DATASET_PATH + "pulmonary/mp3/sick");
    protected static final File UNSICK_FOLDER_PULMONARY = new File(DATASET_PATH + "pulmonary/mp3/unsick");
    protected static final File SICK_FOLDER_THINKLABS = new File(DATASET_PATH + "thinklabs/mp3/sick");
    protected static final File UNSICK_FOLDER_THINKLABS = new File(DATASET_PATH + "thinklabs/mp3/unsick");
    protected static final File SICK_FOLDER_CUSTOM = new File(DATASET_PATH + "custom/sick");
    protected static final File UNSICK_FOLDER_CUSTOM_PART1 = new File(DATASET_PATH + "custom/unsick/part1");
    protected static final File UNSICK_FOLDER_CUSTOM_PART2 = new File(DATASET_PATH + "custom/unsick/part2");
    protected static final File SICK_FOLDER_3MEGYPT_1KHZ = new File(DATASET_1_KHZ_PATH + "3megypt/sick");
    protected static final File UNSICK_FOLDER_3MEGYPT_1KHZ = new File(DATASET_1_KHZ_PATH + "3megypt/unsick");
    protected static final File SICK_FOLDER_PULMONARY_1KHZ = new File(DATASET_1_KHZ_PATH + "pulmonary/sick");
    protected static final File UNSICK_FOLDER_PULMONARY_1KHZ = new File(DATASET_1_KHZ_PATH + "pulmonary/unsick");
    protected static final File SICK_FOLDER_THINKLABS_1KHZ = new File(DATASET_1_KHZ_PATH + "thinklabs/sick");
    protected static final File UNSICK_FOLDER_THINKLABS_1KHZ = new File(DATASET_1_KHZ_PATH + "thinklabs/unsick");
    protected static final File SICK_FOLDER_CUSTOM_1KHZ = new File(DATASET_1_KHZ_PATH + "custom/sick");
    protected static final File UNSICK_FOLDER_CUSTOM_1KHZ = new File(DATASET_1_KHZ_PATH + "custom/unsick");

    protected Map<String, double[]> sickSpectrum = new HashMap<>();
    protected Map<String, double[]> unSickSpectrum = new HashMap<>();
    private int windowSize;

    private int fftSample;
    protected boolean useFilteredRecords;

    public DataSetProviderImpl(int fftSample, int windowSize, boolean useFilteredRecords) {
        this.fftSample = fftSample;
        this.windowSize = windowSize;
        this.useFilteredRecords = useFilteredRecords;
    }

    protected abstract void clearData();

    public abstract T createTestDataSet();

    public abstract T createDataSet();

    public List<LungsSoundClass> getLungsSoundClasses() {
        throw new UnsupportedOperationException();
    }

    protected void calculateSpectrum(File folder, Map<String, double[]> spectrumMap) {
        FilenameFilter filenameFilter = (dir, name) -> (name.toLowerCase().endsWith(".wav")
                || name.toLowerCase().endsWith(".mp3"));
        for (File file : folder.listFiles(filenameFilter)) {
            try {
                double[] spectrum = toDoubleArray(AudioDataExtractor.getAvgSpectrum(file, fftSample, windowSize));
                spectrumMap.put(file.getParent() + "/" + file.getName(), spectrum);
            } catch (Exception e) {
                e.printStackTrace();
                System.err.println("at " + file.getAbsolutePath());
            }
        }
    }

    public void calculateSpectrumTrain() {
//        calculateSpectrum(SICK_FOLDER, sickSpectrum);
//        calculateSpectrum(UNSICK_FOLDER, unSickSpectrum);
//        calculateSpectrum(UNSICK_FOLDER, unSickSpectrum);

        if (useFilteredRecords) {
            calculateSpectrum(SICK_FOLDER_3MEGYPT_1KHZ, sickSpectrum);
            calculateSpectrum(SICK_FOLDER_PULMONARY_1KHZ, sickSpectrum);
//            calculateSpectrum(SICK_FOLDER_THINKLABS_1KHZ, sickSpectrum);
            calculateSpectrum(SICK_FOLDER_CUSTOM_1KHZ, sickSpectrum);

            calculateSpectrum(UNSICK_FOLDER_3MEGYPT_1KHZ, unSickSpectrum);
            calculateSpectrum(UNSICK_FOLDER_PULMONARY_1KHZ, unSickSpectrum);
//            calculateSpectrum(UNSICK_FOLDER_THINKLABS_1KHZ, unSickSpectrum);
            calculateSpectrum(UNSICK_FOLDER_CUSTOM_1KHZ, unSickSpectrum);

        } else {
            calculateSpectrum(SICK_FOLDER_3MEGYPT, sickSpectrum);
            calculateSpectrum(SICK_FOLDER_PULMONARY, sickSpectrum);
//            calculateSpectrum(SICK_FOLDER_THINKLABS, sickSpectrum);
            calculateSpectrum(SICK_FOLDER_CUSTOM, sickSpectrum);

            calculateSpectrum(UNSICK_FOLDER_3MEGYPT, unSickSpectrum);
            calculateSpectrum(UNSICK_FOLDER_PULMONARY, unSickSpectrum);
//            calculateSpectrum(UNSICK_FOLDER_THINKLABS, unSickSpectrum);
            calculateSpectrum(UNSICK_FOLDER_CUSTOM_PART1, unSickSpectrum);
        }
    }

    public void calculateSpectrumTest() {
//        calculateSpectrum(TEST_SICK_DATA_FOLDER, sickSpectrum);

        if (useFilteredRecords) {
//            calculateSpectrum(SICK_FOLDER_3MEGYPT_1KHZ, sickSpectrum);
//            calculateSpectrum(SICK_FOLDER_PULMONARY_1KHZ, sickSpectrum);
            calculateSpectrum(SICK_FOLDER_THINKLABS_1KHZ, sickSpectrum);

//            calculateSpectrum(UNSICK_FOLDER_3MEGYPT_1KHZ, unSickSpectrum);
//            calculateSpectrum(UNSICK_FOLDER_PULMONARY_1KHZ, unSickSpectrum);
            calculateSpectrum(UNSICK_FOLDER_THINKLABS_1KHZ, unSickSpectrum);
        } else {
//            calculateSpectrum(SICK_FOLDER_3MEGYPT, sickSpectrum);
//            calculateSpectrum(SICK_FOLDER_PULMONARY, sickSpectrum);
            calculateSpectrum(SICK_FOLDER_THINKLABS, sickSpectrum);

//            calculateSpectrum(UNSICK_FOLDER_3MEGYPT, unSickSpectrum);
//            calculateSpectrum(UNSICK_FOLDER_PULMONARY, unSickSpectrum);
            calculateSpectrum(UNSICK_FOLDER_THINKLABS, unSickSpectrum);
            calculateSpectrum(UNSICK_FOLDER_CUSTOM_PART2, unSickSpectrum);

        }
    }

    private double[] toDoubleArray(float[] floatArray) {
        double[] doubleArray = new double[floatArray.length];
        for (int i = 0; i < floatArray.length; i++) {
            doubleArray[i] = floatArray[i];
        }
        return doubleArray;
    }
}
