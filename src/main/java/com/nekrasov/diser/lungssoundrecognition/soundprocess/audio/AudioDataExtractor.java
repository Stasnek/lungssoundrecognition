package com.nekrasov.diser.lungssoundrecognition.soundprocess.audio;


import com.nekrasov.diser.lungssoundrecognition.soundprocess.audio.analysis.FFT;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.audio.decoders.Decoder;
import org.apache.commons.lang3.ArrayUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.nekrasov.diser.lungssoundrecognition.soundprocess.audio.DataExtractorUtils.*;

/**
 *
 */
public class AudioDataExtractor {

    private static final int SAMPLE_RATE = 44100;

    public static float[] getRectifiedSpectralFluxFiltered(File file, int fftSample) throws Exception {
        System.out.println();
        Decoder decoder = getDecoder(file);
        FFT fft = new FFT(fftSample, SAMPLE_RATE);
        float[] samplesBuffer = new float[fftSample];
        List<Float> samples = new ArrayList<>();
        List<Float> filteredSamples = new ArrayList<>();

        readAllSamples(decoder, samplesBuffer, samples);
        filterSamples(samples, filteredSamples);
        List<Float> spectralFlux = processSpectralFlux(fftSample, fft, filteredSamples);

        System.out.println("fftSample: " + fftSample);

        return ArrayUtils.toPrimitive(spectralFlux.toArray(new Float[0]));
    }


    public static float[] getRectifiedSpectralFlux(File file, int fftSample) throws Exception {
        System.out.println();
        Decoder decoder = getDecoder(file);
        FFT fft = new FFT(fftSample, SAMPLE_RATE);
        float[] samples = new float[fftSample];
        float[] spectrum = new float[fftSample / 2 + 1];
        float[] lastSpectrum = new float[fftSample / 2 + 1];
        List<Float> spectralFlux = new ArrayList<>();

        while (decoder.readSamples(samples) > 0) {
            fft.forward(samples);
            System.arraycopy(spectrum, 0, lastSpectrum, 0, spectrum.length);
            System.arraycopy(fft.getSpectrum(), 0, spectrum, 0, spectrum.length);

            float flux = 0;
            for (int i = 0; i < spectrum.length; i++) {
                float value = (spectrum[i] - lastSpectrum[i]);
                flux += value < 0 ? 0 : value;
            }
            spectralFlux.add(flux);
        }
        System.out.println("fftSample: " + fftSample);
        System.out.println("spectrumFlux: " + spectralFlux.size());
        return ArrayUtils.toPrimitive(spectralFlux.toArray(new Float[0]));
    }


    public static float[] getRectifiedSpectralFluxAvgFiltered(File file, int fftSample, int windowSize) throws Exception {
        System.out.println();
        Decoder decoder = getDecoder(file);
        FFT fft = new FFT(fftSample, SAMPLE_RATE);
        List<Float> samples = new ArrayList<>();

        float[] samplesBuffer = new float[fftSample];
        List<Float> filteredSamples = new ArrayList<>();

        System.out.println("fftSample: " + fftSample);
        readAllSamples(decoder, samplesBuffer, samples);
        filterSamples(samples, filteredSamples);
        List<Float> spectralFlux = processSpectralFlux(fftSample, fft, filteredSamples);

        return calculateAgvSpectrum(windowSize, spectralFlux);
    }


    public static float[] getRectifiedSpectralFluxAvg(File file, int fftSample, int windowSize) throws Exception {
        System.out.println();
        Decoder decoder = getDecoder(file);
        FFT fft = new FFT(fftSample, SAMPLE_RATE);
        float[] samples = new float[fftSample];
        float[] spectrum = new float[fftSample / 2 + 1];
        float[] lastSpectrum = new float[fftSample / 2 + 1];
        List<Float> spectralFlux = new ArrayList<>();

        while (decoder.readSamples(samples) > 0) {
            fft.forward(samples);
            System.arraycopy(spectrum, 0, lastSpectrum, 0, spectrum.length);
            System.arraycopy(fft.getSpectrum(), 0, spectrum, 0, spectrum.length);

            float flux = 0;
            for (int i = 0; i < spectrum.length; i++) {
                float value = (spectrum[i] - lastSpectrum[i]);
                flux += value < 0 ? 0 : value;
            }
            spectralFlux.add(flux);
        }
        System.out.println("fftSample: " + fftSample);
        System.out.println("spectrumFlux: " + spectralFlux.size());

        return calculateAgvSpectrum(windowSize, spectralFlux);
    }


    public static float[] getAvgSamples(File file, int fftSample, int windowSize) throws Exception {
        System.out.println();
        Decoder decoder = getDecoder(file);
        float[] samplesBuffer = new float[fftSample];
        List<Float> samples = new ArrayList<>();

        readAllSamples(decoder, samplesBuffer, samples);

        return calculateAgvSpectrum(windowSize, samples);
    }


    public static float[] getAvgSamplesFiltered(File file, int fftSample, int windowSize) throws Exception {
        System.out.println();
        Decoder decoder = getDecoder(file);
        float[] samplesBuffer = new float[fftSample];
        List<Float> samples = new ArrayList<>();
        List<Float> filteredSamples = new ArrayList<>();

        readAllSamples(decoder, samplesBuffer, samples);
        filterSamples(samples, filteredSamples);

        return calculateAgvSpectrum(windowSize, filteredSamples);
    }


    public static float[] getAvgSpectrum(File file, int fftSample, int windowSize) throws Exception {
        System.out.println();
        Decoder decoder = getDecoder(file);
        FFT fft = new FFT(fftSample, SAMPLE_RATE);
        float[] samplesBuffer = new float[fftSample];
        List<Float> spectrum = new ArrayList<>();
        List<Float> samples = new ArrayList<>();

        readAllSamples(decoder, samplesBuffer, samples);
        processFFT(fftSample, fft, spectrum, samples);

        return calculateAgvSpectrum(windowSize, spectrum);
    }


    public static float[] getAvgSpectrumFiltered(File file, int fftSample, int windowSize) throws Exception {
        System.out.println();
        Decoder decoder = getDecoder(file);
        FFT fft = new FFT(fftSample, SAMPLE_RATE);
        float[] samplesBuffer = new float[fftSample];
        List<Float> spectrum = new ArrayList<>();
        List<Float> samples = new ArrayList<>();
        List<Float> filteredSamples = new ArrayList<>();

        readAllSamples(decoder, samplesBuffer, samples);
        filterSamples(samples, filteredSamples);
        processFFT(fftSample, fft, spectrum, filteredSamples);

        return calculateAgvSpectrum(windowSize, spectrum);
    }


    public static float[] getMlpSpectrumFiltered(File file, int fftSample, int windowSize) throws Exception {
        System.out.println();
        Decoder decoder = getDecoder(file);
        FFT fft = new FFT(fftSample, SAMPLE_RATE);
        float[] samplesBuffer = new float[fftSample];
        List<Float> spectrum = new ArrayList<>();
        List<Float> samples = new ArrayList<>();
        List<Float> filteredSamples = new ArrayList<>();

        readAllSamples(decoder, samplesBuffer, samples);
        filterSamples(samples, filteredSamples);
        processFFT(fftSample, fft, spectrum, filteredSamples);

        return calculateMlpSpectrum(windowSize, spectrum);
    }


    public static float[] getSumSpectrumFiltered(File file, int fftSample, int windowSize) throws Exception {
        System.out.println();
        Decoder decoder = getDecoder(file);
        FFT fft = new FFT(fftSample, SAMPLE_RATE);
        float[] samplesBuffer = new float[fftSample];
        List<Float> spectrum = new ArrayList<>();
        List<Float> samples = new ArrayList<>();
        List<Float> filteredSamples = new ArrayList<>();

        readAllSamples(decoder, samplesBuffer, samples);
        filterSamples(samples, filteredSamples);
        processFFT(fftSample, fft, spectrum, filteredSamples);

        return calculateSumSpectrum(windowSize, spectrum);
    }
}
