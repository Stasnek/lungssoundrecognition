package com.nekrasov.diser.lungssoundrecognition.soundprocess.nets.encog;

import org.encog.ml.data.MLDataPair;
import org.encog.ml.data.basic.BasicMLDataSet;

import java.util.ArrayList;
import java.util.List;

public class DataSet {

    private BasicMLDataSet dataSet;
    private List<String> labels = new ArrayList<String>();

    public DataSet() {
        this.dataSet = new BasicMLDataSet();
    }

    public void add(MLDataPair theData) {
        dataSet.add(theData);
    }

    public BasicMLDataSet getDataSet() {
        return dataSet;
    }


    public boolean addLabel(String s) {
        return labels.add(s);
    }

    public List<String> getLabels() {
        return labels;
    }


    public void clear() {
        dataSet = new BasicMLDataSet();
        labels = new ArrayList<String>();
    }
}
