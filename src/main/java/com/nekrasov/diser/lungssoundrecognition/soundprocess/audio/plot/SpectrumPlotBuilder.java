package com.nekrasov.diser.lungssoundrecognition.soundprocess.audio.plot;

import com.nekrasov.diser.lungssoundrecognition.soundprocess.audio.AudioDataExtractor;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.utils.NetworkUtils;


import java.io.File;
import java.io.FilenameFilter;
import java.util.HashMap;
import java.util.Map;

import static com.nekrasov.diser.lungssoundrecognition.soundprocess.audio.plot.PlotUtils.buildPlotImage;
import static com.nekrasov.diser.lungssoundrecognition.soundprocess.audio.plot.PlotUtils.buildCombinedPlotImage;

public class SpectrumPlotBuilder {

    private static final String DATASET_PATH = "audio/multi-class-dataset";
    private static final FilenameFilter FILENAME_FILTER = (dir, name) -> (name.toLowerCase().endsWith(".wav")
            || name.toLowerCase().endsWith(".mp3"));

    public static void main(String[] args) throws Exception {
        int[] fftSamples = new int[]{16, 32, 64, 128, 256};
        String[] classes = new String[]{"rhonchi", "crackles", "rub", "stridor", "wheezes", "normal"};

        for (int fftSample : fftSamples) {
            spectrumAvgFiltered(classes, fftSample);
        }
    }


    //combined
    private static void buildClassCombinedPlot(String[] classes, int fftSample) throws Exception {
        for (String audioClass : classes) {
            Map<String, float[]> classSpectrum = new HashMap<>();
            File classRoot = new File(DATASET_PATH, audioClass);
            for (File file : getFiles(classRoot)) {
                float[] spectrum = AudioDataExtractor.getAvgSpectrumFiltered(file,
                        fftSample, fftSample);
                classSpectrum.put(file.getName(), NetworkUtils.getNormalizedData(spectrum));
            }
            buildCombinedPlotImage(classSpectrum, "combinedPlots_" + fftSample, audioClass);
        }
    }


    //aggregate
    private static void buildAggregatedPlot(String[] classes, int fftSample) throws Exception {
        for (String audioClass : classes) {
            File classRoot = new File(DATASET_PATH, audioClass);
            for (File file : getFiles(classRoot)) {
                Map<String, float[]> plotData = new HashMap<>();
                plotData.put("AvgSpectrumFiltered", AudioDataExtractor.getAvgSpectrumFiltered(file,
                        fftSample, fftSample));
                plotData.put("AvgSpectrum", AudioDataExtractor.getAvgSpectrum(file,
                        fftSample, fftSample));
                buildPlotImage(file, plotData, "aggregatedAvgPlots_" + fftSample);
            }
        }
    }


    private static File[] getFiles(File classRoot) {
        return classRoot.listFiles(FILENAME_FILTER);
    }


    //    // sum
//    private static void spectrumSumFiltered(String[] classes, int fftSample) throws Exception {
//        for (String audioClass : classes) {
//            File classRoot = new File(DATASET_PATH, audioClass);
//            for (File file : getFiles(classRoot)) {
//                float[] avgSpectrum = AudioDataExtractor.getSumSpectrumFiltered(file, fftSample, fftSample);
//                buildPlotImage(file, avgSpectrum, "spectrumSumFiltered_11_fft_" + fftSample);
//            }
//        }
//    }
//
//
//
//    // mlp
//    private static void spectrumMlpFiltered(String[] classes, int fftSample) throws Exception {
//        for (String audioClass : classes) {
//            File classRoot = new File(DATASET_PATH, audioClass);
//            for (File file : getFiles(classRoot)) {
//                float[] avgSpectrum = AudioDataExtractor.getMlpSpectrumFiltered(file, fftSample, fftSample);
//                buildPlotImage(file, avgSpectrum, "spectrumMlpFiltered_11_fft_" + fftSample);
//            }
//        }
//    }
//
//
//    // avg
//    private static void rectifiedSpectralFluxAvgFiltered(String[] classes, int fftSample) throws Exception {
//        for (String audioClass : classes) {
//            File classRoot = new File(DATASET_PATH, audioClass);
//            for (File file : getFiles(classRoot)) {
//                float[] avgSpectrum = AudioDataExtractor.getRectifiedSpectralFluxAvgFiltered(file,
//                        fftSample, fftSample);
//                buildPlotImage(file, avgSpectrum, "rectifiedSpectralFluxAvgFiltered_fft_" + fftSample);
//            }
//        }
//    }
//
//    private static void rectifiedSpectralFluxAvgFilteredWindow(String[] classes, int fftSample) throws Exception {
//        for (String audioClass : classes) {
//            File classRoot = new File(DATASET_PATH, audioClass);
//            for (File file : getFiles(classRoot)) {
//                float[] avgSpectrum = AudioDataExtractor.getRectifiedSpectralFluxAvgFiltered(file, 128,
//                        fftSample);
//                buildPlotImage(file, avgSpectrum, "rectifiedSpectralFluxAvgFiltered_fft_128_window_" + fftSample);
//            }
//        }
//    }
//
//
//    private static void rectifiedSpectralFluxFiltered(String[] classes, int fftSample) throws Exception {
//        for (String audioClass : classes) {
//            File classRoot = new File(DATASET_PATH, audioClass);
//            for (File file : getFiles(classRoot)) {
//                float[] avgSpectrum = AudioDataExtractor.getRectifiedSpectralFluxFiltered(file, fftSample);
//                buildPlotImage(file, avgSpectrum, "rectifiedSpectralFluxFiltered_fft_" + fftSample);
//            }
//        }
//    }
//
//
//    private static void rectifiedSpectralFlux(String[] classes, int fftSample) throws Exception {
//        for (String audioClass : classes) {
//            File classRoot = new File(DATASET_PATH, audioClass);
//            for (File file : getFiles(classRoot)) {
//                float[] avgSpectrum = AudioDataExtractor.getRectifiedSpectralFlux(file, fftSample);
//                buildPlotImage(file, avgSpectrum, "rectifiedSpectralFlux_fft_" + fftSample);
//            }
//        }
//    }
//
//
//    private static void rectifiedSpectralFluxAvgWindow(String[] classes, int fftSample) throws Exception {
//        for (String audioClass : classes) {
//            File classRoot = new File(DATASET_PATH, audioClass);
//            for (File file : getFiles(classRoot)) {
//                float[] avgSpectrum = AudioDataExtractor.getRectifiedSpectralFluxAvg(file, 128, fftSample);
//                buildPlotImage(file, avgSpectrum, "rectifiedSpectralFluxAvg_fft_128_Window_" + fftSample);
//            }
//        }
//    }
//
//
//    private static void rectifiedSpectralFluxAvg(String[] classes, int fftSample) throws Exception {
//        for (String audioClass : classes) {
//            File classRoot = new File(DATASET_PATH, audioClass);
//            for (File file : getFiles(classRoot)) {
//                float[] avgSpectrum = AudioDataExtractor.getRectifiedSpectralFluxAvg(file, fftSample, fftSample);
//                buildPlotImage(file, avgSpectrum, "rectifiedSpectralFluxAvg_fft_" + fftSample);
//            }
//        }
//    }
//
//    private static void samplesAvg(String[] classes, int fftSample) throws Exception {
//        for (String audioClass : classes) {
//            File classRoot = new File(DATASET_PATH, audioClass);
//            for (File file : getFiles(classRoot)) {
//                float[] avgSpectrum = AudioDataExtractor.getAvgSamples(file, fftSample, fftSample);
//                buildPlotImage(file, avgSpectrum, "samplesAvg_fft_" + fftSample);
//            }
//        }
//    }
//
//    private static void samplesAvgFiltered(String[] classes, int fftSample) throws Exception {
//        for (String audioClass : classes) {
//            File classRoot = new File(DATASET_PATH, audioClass);
//            for (File file : getFiles(classRoot)) {
//                float[] avgSpectrum = AudioDataExtractor.getAvgSamplesFiltered(file, fftSample, fftSample);
//                buildPlotImage(file, avgSpectrum, "samplesAvgFiltered_fft_" + fftSample);
//            }
//        }
//    }
//
//    private static void spectrumAvg(String[] classes, int fftSample) throws Exception {
//        for (String audioClass : classes) {
//            File classRoot = new File(DATASET_PATH, audioClass);
//            for (File file : getFiles(classRoot)) {
//                float[] avgSpectrum = AudioDataExtractor.getAvgSpectrum(file, fftSample, fftSample);
//                buildPlotImage(file, avgSpectrum, "spectrumAvg_fft_" + fftSample);
//            }
//        }
//    }
//
    private static void spectrumAvgFiltered(String[] classes, int fftSample) throws Exception {
        for (String audioClass : classes) {
            File classRoot = new File(DATASET_PATH, audioClass);
            for (File file : getFiles(classRoot)) {
                float[] avgSpectrum = AudioDataExtractor.getAvgSpectrumFiltered(file, fftSample, fftSample);
                buildPlotImage(file, NetworkUtils.getNormalizedData(avgSpectrum),
                        "spectrumAvgFiltered_fft_" + fftSample);
            }
        }
    }
}
