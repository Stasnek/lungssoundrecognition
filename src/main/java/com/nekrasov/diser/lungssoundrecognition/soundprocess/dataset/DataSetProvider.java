package com.nekrasov.diser.lungssoundrecognition.soundprocess.dataset;

import com.nekrasov.diser.lungssoundrecognition.soundprocess.lungssoundclass.LungsSoundClass;

import java.util.List;

public interface DataSetProvider<T> {

    T createTestDataSet();

    T createDataSet();

    List<LungsSoundClass> getLungsSoundClasses();
}
