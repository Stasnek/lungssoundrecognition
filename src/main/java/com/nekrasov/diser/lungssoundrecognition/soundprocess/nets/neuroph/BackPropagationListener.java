package com.nekrasov.diser.lungssoundrecognition.soundprocess.nets.neuroph;

import org.neuroph.core.events.LearningEvent;
import org.neuroph.core.events.LearningEventListener;
import org.neuroph.nnet.learning.BackPropagation;

/**
 *
 */
public class BackPropagationListener implements LearningEventListener {

    public void handleLearningEvent(LearningEvent event) {
        BackPropagation bp = (BackPropagation) event.getSource();
        if (bp.getCurrentIteration() % 100 == 0) {
            if (event.getEventType() != LearningEvent.Type.LEARNING_STOPPED) {
                System.out.println(bp.getCurrentIteration() + ". iteration : " + bp.getTotalNetworkError());
            }
//            if (bp.getCurrentIteration() > maxIterations) {
//                bp.stopLearning();
//                System.out.println("learn failed");
//            }
        }
    }
}
