package com.nekrasov.diser.lungssoundrecognition.soundprocess.lungssoundclass;

import java.io.File;

public class AudioFileSpectrum {
    private File file;
    private double[] spectrum;

    public AudioFileSpectrum(File file, double[] spectrum) {
        this.file = file;
        this.spectrum = spectrum;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public double[] getSpectrum() {
        return spectrum;
    }

    public void setSpectrum(double[] spectrum) {
        this.spectrum = spectrum;
    }
}
