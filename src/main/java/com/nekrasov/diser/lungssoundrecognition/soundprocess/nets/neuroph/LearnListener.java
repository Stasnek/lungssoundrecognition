package com.nekrasov.diser.lungssoundrecognition.soundprocess.nets.neuroph;

import org.neuroph.nnet.learning.BackPropagation;

import java.util.concurrent.TimeUnit;

public class LearnListener implements Runnable {
    private final BackPropagation learningRule;
    private final Thread thread;

    public LearnListener(BackPropagation learningRule) {
        this.learningRule = learningRule;
        thread = new Thread(this);
        thread.setDaemon(true);
    }

    @Override
    public void run() {
        while (!learningRule.isStopped()) {
            if (learningRule.getCurrentIteration() % 100 == 0) {
                System.out.println(learningRule.getCurrentIteration() + ". iteration : "
                        + learningRule.getTotalNetworkError());
            }
            try {
                TimeUnit.MILLISECONDS.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void startTask() {
        thread.start();
    }
}
