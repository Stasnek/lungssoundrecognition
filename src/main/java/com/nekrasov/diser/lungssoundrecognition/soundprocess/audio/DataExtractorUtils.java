package com.nekrasov.diser.lungssoundrecognition.soundprocess.audio;

import com.nekrasov.diser.lungssoundrecognition.soundprocess.audio.analysis.FFT;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.audio.decoders.Decoder;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.audio.decoders.impl.MP3Decoder;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.audio.decoders.impl.WaveDecoder;
import org.apache.commons.lang3.ArrayUtils;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class DataExtractorUtils {
    private DataExtractorUtils() {
    }

    static float[] calculateAgvSpectrum(int windowSize, List<Float> spectrum) {
        float[] spectrumAVG = new float[windowSize];
        int windowsAmount = getWindowsAmount(windowSize, spectrum.size());
        int modulo = spectrum.size() % windowsAmount;

        System.out.println("windowSize: " + windowSize);
        System.out.println("windowsAmount: " + windowsAmount);

        for (int i = 0; i < windowSize; i++) {
            for (int j = i; j < spectrum.size(); j += windowSize) {
                spectrumAVG[i] += Math.abs(spectrum.get(j));
            }
            if (i < modulo) {
                spectrumAVG[i] /= windowsAmount + 1;
            } else {
                spectrumAVG[i] /= windowsAmount;
            }
        }
        return spectrumAVG;
    }


    static int getWindowsAmount(int windowSize, int spectrumSize) {
        return spectrumSize < windowSize ? 1 : spectrumSize / windowSize;
    }


    static float[] calculateMlpSpectrum(int windowSize, List<Float> spectrum) {
        float[] spectrumMLP = new float[windowSize];
        int windowsAmount = getWindowsAmount(windowSize, spectrum.size());
        for (int i = 0; i < spectrumMLP.length; i++) {
            spectrumMLP[i] = 1;
        }

        System.out.println("windowSize: " + windowSize);
        System.out.println("windowsAmount: " + windowsAmount);

        for (int i = 0; i < windowSize; i++) {
            for (int j = i; j < spectrum.size(); j += windowSize) {
                spectrumMLP[i] *= Math.abs(spectrum.get(j));
            }
        }
        return spectrumMLP;
    }


    static float[] calculateSumSpectrum(int windowSize, List<Float> spectrum) {
        float[] spectrumSUM = new float[windowSize];
        int windowsAmount = getWindowsAmount(windowSize, spectrum.size());

        System.out.println("windowSize: " + windowSize);
        System.out.println("windowsAmount: " + windowsAmount);

        for (int i = 0; i < windowSize; i++) {
            for (int j = i; j < spectrum.size(); j += windowSize) {
                spectrumSUM[i] += Math.abs(spectrum.get(j));
            }
        }
        return spectrumSUM;
    }


    static void processFFT(int fftSample, FFT fft, List spectrum, List<Float> filteredSamples) {
        for (int i = 0; i + fftSample < filteredSamples.size(); i += fftSample) {
            fft.forward(ArrayUtils.toPrimitive(filteredSamples.subList(i, i + fftSample).toArray(new Float[fftSample])));
            Collections.addAll(spectrum, ArrayUtils.toObject(fft.getSpectrum()));
        }
        System.out.println("spectrum: " + spectrum.size());
    }


    static List<Float> processSpectralFlux(int fftSample, FFT fft, List<Float> filteredSamples) {
        float[] lastSpectrum = new float[fftSample / 2 + 1];
        float[] spectrum = new float[fftSample / 2 + 1];
        List<Float> spectralFlux = new ArrayList<>();

        for (int i = 0; i + fftSample < filteredSamples.size(); i += fftSample) {
            fft.forward(ArrayUtils.toPrimitive(filteredSamples.subList(i, i + fftSample).toArray(new Float[fftSample])));
            System.arraycopy(spectrum, 0, lastSpectrum, 0, spectrum.length);
            System.arraycopy(fft.getSpectrum(), 0, spectrum, 0, spectrum.length);

            float flux = 0;
            for (int k = 0; k < spectrum.length; k++) {
                float value = (spectrum[k] - lastSpectrum[k]);
                flux += value < 0 ? 0 : value;
            }
            spectralFlux.add(flux);
        }
        System.out.println("spectrumFlux: " + spectralFlux.size());
        return spectralFlux;
    }


    static void filterSamples(List<Float> samples, List<Float> filteredSamples) {
        for (int i = 0; i < samples.size(); i += 11) {
            filteredSamples.add(samples.get(i));
        }
        System.out.println("filteredSamples: " + filteredSamples.size());
    }


    static void readAllSamples(Decoder decoder, float[] samplesBuffer, List<Float> samples) {
        while (decoder.readSamples(samplesBuffer) > 0) {
            Collections.addAll(samples, ArrayUtils.toObject(samplesBuffer));
        }
        System.out.println("Samples: " + samples.size());
    }


    static Decoder getDecoder(File file) throws Exception {
        Decoder decoder;
        System.out.println("File: " + file.getName());
        if (file.getName().contains(".wav") || file.getName().contains(".WAV")) {
            decoder = new WaveDecoder(new FileInputStream(file));
        } else {
            decoder = new MP3Decoder(new FileInputStream(file));
        }
        return decoder;
    }
}
