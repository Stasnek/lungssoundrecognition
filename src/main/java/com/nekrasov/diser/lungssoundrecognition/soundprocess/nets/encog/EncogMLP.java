package com.nekrasov.diser.lungssoundrecognition.soundprocess.nets.encog;

import com.nekrasov.diser.lungssoundrecognition.soundprocess.dataset.DataSetProvider;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.nets.NeuralNet;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.report.NetworkReport;
import org.encog.Encog;
import org.encog.engine.network.activation.ActivationSigmoid;
import org.encog.ml.data.MLData;
import org.encog.ml.data.MLDataPair;
import org.encog.ml.data.MLDataSet;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.layers.BasicLayer;
import org.encog.neural.networks.training.propagation.back.Backpropagation;
import org.encog.persist.EncogDirectoryPersistence;

import java.io.File;
import java.util.Iterator;
import java.util.List;


public class EncogMLP implements NeuralNet {
    private BasicNetwork network;
    private MLDataSet trainingSet;
    private DataSetProvider dataSetProvider;
    private DataSet dataSet;
    private String name;
    private File savedNetwork;
    private double maxError = 0.001;
    private Backpropagation train;
    private NetworkReport networkReport;

    private double trainingTime;

    public EncogMLP(DataSetProvider dataSetProvider, String name) {
        networkReport = new NetworkReport();
        this.name = name;
        this.dataSetProvider = dataSetProvider;
        savedNetwork = new File(name + ".net");
        dataSet = (DataSet) this.dataSetProvider.createDataSet();
        trainingSet = dataSet.getDataSet();
        // create a neural network, without using a factory
        network = buildNetwork();
        train = new Backpropagation(network, trainingSet);
    }

    private BasicNetwork buildNetwork() {
        BasicNetwork network = new BasicNetwork();
        int inputSize = trainingSet.getInputSize();
        network.addLayer(new BasicLayer(null, true, inputSize));
        network.addLayer(new BasicLayer(new ActivationSigmoid(), true, inputSize));
        network.addLayer(new BasicLayer(new ActivationSigmoid(), true, inputSize));
        network.addLayer(new BasicLayer(new ActivationSigmoid(), true, inputSize));
        network.addLayer(new BasicLayer(new ActivationSigmoid(), true, inputSize));
        network.addLayer(new BasicLayer(new ActivationSigmoid(), true, inputSize));

        network.addLayer(new BasicLayer(new ActivationSigmoid(), true, inputSize / 2));
        network.addLayer(new BasicLayer(new ActivationSigmoid(), true, inputSize / 4));
        network.addLayer(new BasicLayer(new ActivationSigmoid(), true, inputSize / 8));
        network.addLayer(new BasicLayer(new ActivationSigmoid(), false, trainingSet.getIdealSize()));

        network.getStructure().finalizeStructure();
        network.reset();
        return network;
    }


    public void train() {
        // train the neural network

        int epoch = 1;
        System.out.println("Learning rate: " + train.getLearningRate());
        networkReport.notchStartTime();
//        train.setBatchSize(20);
        do {
            train.iteration();
            if (epoch % 100 == 0) {
                System.out.println("Epoch #" + epoch + " Error:" + train.getError());
            }
            epoch++;
        } while (train.getError() > maxError);
        System.out.println("Epoch #" + epoch + " Error:" + train.getError());
        System.out.println("Iteration: " + epoch);
        networkReport.notchEndTime("Training");
        train.finishTraining();

        // save network
//        EncogDirectoryPersistence.saveObject(savedNetwork, network);
//        test(dataSet);
    }

    public void test() {
        networkReport = new NetworkReport();
        DataSet testDataSet = (DataSet) dataSetProvider.createTestDataSet();
        test(testDataSet);
        networkReport.dumpResult();
    }

    public void test(DataSet dataSet) {
        // test the neural network
        List<String> labels = dataSet.getLabels();
        Iterator<String> labelsIterator = labels.iterator();
        networkReport.notchStartTime();
        for (MLDataPair pair : dataSet.getDataSet()) {
            final MLData output = network.compute(pair.getInput());
            final String label = labelsIterator.next();
            networkReport.addRowToResult(output.getData(), pair.getIdealArray(), label);
        }
        networkReport.notchEndTime("Testing");
        Encog.getInstance().shutdown();
        networkReport.createAndPrintReport();
    }

    public void setLearningRate(double learningRate) {
        train.setLearningRate(learningRate);
    }

    public void setBatchMode(boolean batchMode) {

    }

    public void setMaxError(double maxError) {
        this.maxError = maxError;
    }

    public void setMaxIterations(int maxIterations) {
    }

    public double getTrainingTime() {
        return trainingTime;
    }
}