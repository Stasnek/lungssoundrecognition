package com.nekrasov.diser.lungssoundrecognition.soundprocess.nets;

public interface NeuralNet {

    void train();

    void test();

    void setLearningRate(double learningRate);

    void setBatchMode(boolean batchMode);

    void setMaxError(double maxError);

    void setMaxIterations(int maxIterations);

    double getTrainingTime();
}
