package com.nekrasov.diser.lungssoundrecognition.soundprocess.csv;


import com.nekrasov.diser.lungssoundrecognition.soundprocess.lungssoundclass.AudioFileSpectrum;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.lungssoundclass.LungsSoundClass;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.report.NetworkItemResult;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.ArrayUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class CSVHandler {

    public Map<String, List<AudioFileSpectrum>> readCSVFile(File file) throws IOException {
        Map<String, List<AudioFileSpectrum>> classesSpectrum = new HashMap<>();
        Reader in = new FileReader(file);
        Iterable<CSVRecord> records = CSVFormat.RFC4180.withFirstRecordAsHeader().parse(in);
        for (CSVRecord record : records) {
            String className = record.get("className");
            File audioFile = new File(record.get("file"));
            String rawSpectrum = record.get("spectrum");
            Pattern pattern = Pattern.compile(",");
            double[] spectrum = ArrayUtils.toPrimitive(pattern.splitAsStream(rawSpectrum)
                    .map(Double::parseDouble)
                    .toArray(Double[]::new));
            AudioFileSpectrum audioFileSpectrum = new AudioFileSpectrum(audioFile, spectrum);
            putToMap(classesSpectrum, className, audioFileSpectrum);
        }
        return classesSpectrum;
    }

    public void printCsv(List<LungsSoundClass> classes, File file) throws IOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file));
             CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT
                     .withHeader("className", "file", "spectrum"))) {
            for (LungsSoundClass soundClass : classes) {
                String className = soundClass.getClassName();
                for (AudioFileSpectrum audioFileSpectrum : soundClass.getAudioFilesSpectrum()) {
                    csvPrinter.printRecord(className, audioFileSpectrum.getFile().getPath(),
                            arrayToString(audioFileSpectrum.getSpectrum()));
                }
            }
            csvPrinter.flush();
        }
    }


//    public Map<String, List<AudioFileSpectrum>> readCSVResultFile(File file) throws IOException {
//        Map<String, List<AudioFileSpectrum>> classesSpectrum = new HashMap<>();
//        Reader in = new FileReader(file);
//        Iterable<CSVRecord> records = CSVFormat.RFC4180.withFirstRecordAsHeader().parse(in);
//        for (CSVRecord record : records) {
//            String className = record.get("file");
//            String rawSpectrum = record.get("result");
//            putToMap(classesSpectrum, className, audioFileSpectrum);
//        }
//        return classesSpectrum;
//    }

    public void printResultCsv(List<NetworkItemResult> results, File file) throws IOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file, true));
             CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT
                     .withHeader("file", "result"))) {
            for (NetworkItemResult result : results) {
                csvPrinter.printRecord(result.getLabel(), !result.isFailed());
            }
            csvPrinter.flush();
        }
    }

    private String arrayToString(double[] array) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < array.length - 1; i++) {
            builder.append(array[i]);
            builder.append(',');
        }
        builder.append(array[array.length - 1]);
        return builder.toString();
    }

    private void putToMap(Map<String, List<AudioFileSpectrum>> classesSpectrum, String className,
                          AudioFileSpectrum audioFileSpectrum) {
        if (!classesSpectrum.containsKey(className)) {
            classesSpectrum.put(className, new ArrayList<>());
        }
        classesSpectrum.get(className).add(audioFileSpectrum);
    }
}
