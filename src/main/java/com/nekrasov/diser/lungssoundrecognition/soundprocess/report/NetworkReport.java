package com.nekrasov.diser.lungssoundrecognition.soundprocess.report;

import com.nekrasov.diser.lungssoundrecognition.soundprocess.csv.CSVHandler;
import dnl.utils.text.table.TextTable;
import org.apache.commons.lang3.ArrayUtils;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import static java.util.Comparator.comparingDouble;

public class NetworkReport {
    private List<NetworkItemResult> results = new ArrayList<>();
    private double startTime;
    private final DecimalFormat decimalFormat = new DecimalFormat("0.00");

    public void notchStartTime() {
        startTime = System.currentTimeMillis();
    }

    public void notchEndTime(String actionType) {
        System.out.println(actionType + " time is " + (System.currentTimeMillis() - startTime) / 1000 + " s.");
    }

    public void addRowToResult(double[] output, double[] ideal, String label) {
        results.add(new NetworkItemResult(output, ideal, label));
    }

    public void dumpResult() {
        CSVHandler csvHandler = new CSVHandler();
        try {
            csvHandler.printResultCsv(results, new File("target/result.csv"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void createAndPrintReport() {
        System.out.println("Test result:");
        int allRows = results.size();
        int failedCount = analyzeResults();
//        printNetworkOutputResult();
        System.out.println("Total: " + allRows);
        System.out.println("Success: " + (allRows - failedCount));
        System.out.println("Failed: " + failedCount);
        System.out.println("Error: " + decimalFormat.format((double) failedCount * 100 / allRows) + "%");
    }

    private void printNetworkOutputResult() {
        String[] columnNames = new String[]{"Output", "Ideal", "Class info", "Successful"};
        Object[][] tableData = new Object[results.size()][columnNames.length];
        for (int i = 0; i < results.size(); i++) {
            tableData[i][0] = formatArray(results.get(i).getOutput());
            tableData[i][1] = getIndexOfMaxValue(results.get(i).getIdeal()) + 1;
            tableData[i][2] = results.get(i).getLabel();
            tableData[i][3] = !results.get(i).isFailed();
        }
        TextTable textTable = new TextTable(columnNames, tableData);
        textTable.setAddRowNumbering(true);
        textTable.printTable();
    }

    private String formatArray(double[] array) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < array.length; i++) {
            result.append(decimalFormat.format(array[i]));
            result.append(", ");
        }
        return result.toString().substring(0, result.length() - 2);
    }

    private int analyzeResults() {
        int failedCount = 0;
        for (NetworkItemResult result : results) {
            if (!validateResult(result)) {
                failedCount++;
                result.setFailed(true);
            }
        }
        return failedCount;
    }

    private boolean validateResult(NetworkItemResult result) {
        return getIndexOfMaxValue(result.getIdeal()) == getIndexOfMaxValue(result.getOutput());
    }

    private int getIndexOfMaxValue(double[] array) {
        Double[] doubleArray = ArrayUtils.toObject(array);
        List<Double> intArr = Arrays.asList(doubleArray);
        return IntStream.range(0, intArr.size()).boxed()
                .max(comparingDouble(intArr::get)).get();
    }
}
