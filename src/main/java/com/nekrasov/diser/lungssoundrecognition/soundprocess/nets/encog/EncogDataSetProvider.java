package com.nekrasov.diser.lungssoundrecognition.soundprocess.nets.encog;

import com.nekrasov.diser.lungssoundrecognition.soundprocess.dataset.impl.DataSetProviderImpl;
import org.encog.ml.data.MLData;
import org.encog.ml.data.MLDataPair;
import org.encog.ml.data.basic.BasicMLData;
import org.encog.ml.data.basic.BasicMLDataPair;

import java.util.Arrays;
import java.util.Map;

public class EncogDataSetProvider extends DataSetProviderImpl<DataSet> {
    private DataSet dataSet;
    private int windowSize;

    public EncogDataSetProvider(int fftSample, int windowSize, boolean useFilteredRecords) {
        super(fftSample, windowSize, useFilteredRecords);
        this.windowSize = windowSize;
    }

    protected void clearData() {
        if (dataSet != null) {
            dataSet.clear();
        }
        sickSpectrum.clear();
        unSickSpectrum.clear();
    }

    public DataSet createTestDataSet() {
        clearData();
        calculateSpectrumTest();
        fillDataSetRows(true);
        return dataSet;
    }

    public DataSet createDataSet() {
        clearData();
        calculateSpectrumTrain();
        fillDataSetRows(false);
        return dataSet;
    }


    protected void fillDataSetRows(boolean isTest) {
        dataSet = new DataSet();
        addRowsToDataSet(sickSpectrum, windowSize, SICK_OUTPUT, UNSICK_OUTPUT, "---- sick! ----");
        addRowsToDataSet(unSickSpectrum, windowSize, UNSICK_OUTPUT, SICK_OUTPUT, "---- not sick! ----");
    }

    protected void addRowsToDataSet(Map<String, double[]> spectrumMap, int input, double output1,
                                    double output2, String label) {
        for (Map.Entry<String, double[]> spectrum : spectrumMap.entrySet()) {
            MLData inputData = new BasicMLData(Arrays.copyOf(spectrum.getValue(), input));
            MLData outputData = new BasicMLData(new double[]{output1, output2});

            MLDataPair row = new BasicMLDataPair(inputData, outputData);
            dataSet.addLabel(label + " File " + spectrum.getKey());
            dataSet.add(row);
        }
    }
}
