package com.nekrasov.diser.lungssoundrecognition.soundprocess;


import com.nekrasov.diser.lungssoundrecognition.soundprocess.audio.SpectrumType;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.dataset.DataSetProvider;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.lungssoundclass.LungsSoundClass;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.lungssoundclass.NetworkClassBuilder;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.nets.NeuralNet;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.nets.deep4j.DL4jConvolution;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.nets.deep4j.Deep4jMLP;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.nets.deep4j.Dl4jMultiClassDataSetProvider;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.nets.encog.EncogMLP;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.nets.encog.EncogMultiClassDataSetProvider;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.nets.neuroph.ConvolutionNet;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.nets.neuroph.NeurophMLP;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.nets.neuroph.NeurophMultiClassDataSetProvider;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 *
 */
public class LungsSoundRecognition {

    private static List<Integer> testIndexes = Arrays.asList(0);

    public static void main(String[] args) throws IOException {
        int fftSample = 64;
        int windowSize = fftSample;

        double learningRate = 0.005;
        double maxError = 0.005;
        int maxIterations = 3000;
        boolean batchMode = false;

        RunConfiguration runConfiguration = new RunConfiguration(fftSample, windowSize, learningRate, maxError,
                maxIterations, batchMode, SpectrumType.SPECTRUM_AVG_FILTERED);


        for (int i = 0; i < 10; i++) {
            testIndexes = Arrays.asList(i);
            System.out.println("Testing index: " + i);
            for (int j = 0; j < 28; j++) {
                System.out.println("Testing index: " + i);
                System.out.println("Run: " + j);
//                runEncog(runConfiguration);
                runNeurophCNN(runConfiguration);
            }
        }


//        runNeurophCNN(runConfiguration);
//        runDl4jMLP(runConfiguration);
//        runDl4jCNN(runConfiguration);
    }

    private static void runEncog(RunConfiguration conf) throws IOException {
        DataSetProvider encogDataSetProvider
                = new EncogMultiClassDataSetProvider(NetworkClassBuilder.build(), conf.getFftSample(),
                conf.getWindowSize(), conf.getSpectrumType(), testIndexes);
        EncogMLP encogMLP = new EncogMLP(encogDataSetProvider, "EncogMLP");
        runNetwork(encogMLP, conf.getLearningRate(), conf.getMaxError(), conf.getMaxIterations(), conf.isBatchMode());
    }


    private static void runNeurophMLP(DataSetProvider neurophDataSetProvider, double learningRate, double maxError,
                                      int maxIterations, boolean batchMode) {
        NeurophMLP neurophMlp = new NeurophMLP(neurophDataSetProvider, "NeurophMLP", 20);
        runNetwork(neurophMlp, learningRate, maxError, maxIterations, batchMode);
    }


    private static void runDl4jMLP(RunConfiguration conf) throws IOException {
        List<LungsSoundClass> classes = NetworkClassBuilder.build();
        Dl4jMultiClassDataSetProvider dl4jDataSetProvider = new Dl4jMultiClassDataSetProvider(classes,
                conf.getFftSample(), conf.getWindowSize(), conf.getSpectrumType(), testIndexes);
        Deep4jMLP dl4jMLP = new Deep4jMLP(dl4jDataSetProvider, "DL4jMLP", conf.getWindowSize(), classes.size());
        runNetwork(dl4jMLP, conf.getLearningRate(), conf.getMaxError(), conf.getMaxIterations(), conf.isBatchMode());
    }


    private static void runDl4jCNN(RunConfiguration conf) throws IOException {
        List<LungsSoundClass> classes = NetworkClassBuilder.build();
        Dl4jMultiClassDataSetProvider dl4jDataSetProvider = new Dl4jMultiClassDataSetProvider(classes,
                conf.getFftSample(), conf.getWindowSize(), conf.getSpectrumType(), testIndexes);
        int input = (int) Math.sqrt(conf.getWindowSize());
        DL4jConvolution dl4jConvolution = new DL4jConvolution(dl4jDataSetProvider, "DL4jConvolution", input, input,
                classes.size());
        runNetwork(dl4jConvolution, conf.getLearningRate(), conf.getMaxError(), conf.getMaxIterations(), conf.isBatchMode());
    }


    private static void runNeurophCNN(RunConfiguration conf) throws IOException {
        DataSetProvider neurophDataSetProvider
                = new NeurophMultiClassDataSetProvider(NetworkClassBuilder.build(), conf.getFftSample(),
                conf.getWindowSize(), conf.getSpectrumType(), testIndexes);
        int input = (int) Math.sqrt(conf.getWindowSize());
        ConvolutionNet convolutionNet = new ConvolutionNet(neurophDataSetProvider, "convolutionNet",
                input, input);
        runNetwork(convolutionNet, conf.getLearningRate(), conf.getMaxError(), conf.getMaxIterations(),
                conf.isBatchMode());
    }


    private static void runNetwork(NeuralNet neuralNet, double learningRate,
                                   double maxError, int maxIterations, boolean batchMode) {
        neuralNet.setBatchMode(batchMode);
        neuralNet.setLearningRate(learningRate);
        neuralNet.setMaxError(maxError);
        neuralNet.setMaxIterations(maxIterations);

        System.out.println("Train neural network");
        neuralNet.train();

        System.out.println("Testing trained neural network");
        neuralNet.test();
    }
}
