package com.nekrasov.diser.lungssoundrecognition.soundprocess.lungssoundclass;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class LungsSoundClass {
    private final String className;
    private List<AudioFileSpectrum> trainSpectrum = new ArrayList<>();
    private List<AudioFileSpectrum> testSpectrum = new ArrayList<>();
    private List<AudioFileSpectrum> audioFilesSpectrum = new ArrayList<>();
    private File trainSourcesRoot;
    private File testSourcesRoot;
    private double[] output;

    public LungsSoundClass(String className, File trainSourcesRoot,
                           File testSourcesRoot, double[] output) {
        this.className = className;
        this.trainSourcesRoot = trainSourcesRoot;
        this.testSourcesRoot = testSourcesRoot;
        this.output = output;
    }

    public void addAudioFileSpectrum(AudioFileSpectrum audioFileSpectrum) {
        if (audioFilesSpectrum == null) {
            audioFilesSpectrum = new ArrayList<>();
        }
        audioFilesSpectrum.add(audioFileSpectrum);
    }

    public void addRowToTrainSpectrum(AudioFileSpectrum audioFileSpectrum) {
        if (trainSpectrum == null) {
            trainSpectrum = new ArrayList<>();
        }
        trainSpectrum.add(audioFileSpectrum);
    }

    public void addRowToTestSpectum(AudioFileSpectrum audioFileSpectrum) {
        if (testSpectrum == null) {
            testSpectrum = new ArrayList<>();
        }
        testSpectrum.add(audioFileSpectrum);
    }

    public void setTrainSourcesRoot(File trainSourcesRoot) {
        this.trainSourcesRoot = trainSourcesRoot;
    }

    public File getTrainSourcesRoot() {
        return trainSourcesRoot;
    }


    public String getClassName() {
        return className;
    }

    public List<AudioFileSpectrum> getTrainSpectrum() {
        return trainSpectrum;
    }

    public void setTrainSpectrum(List<AudioFileSpectrum> trainSpectrum) {
        this.trainSpectrum = trainSpectrum;
    }

    public List<AudioFileSpectrum> getTestSpectrum() {
        return testSpectrum;
    }

    public void setTestSpectrum(List<AudioFileSpectrum> testSpectrum) {
        this.testSpectrum = testSpectrum;
    }

    public File getTestSourcesRoot() {
        return testSourcesRoot;
    }

    public void setTestSourcesRoot(File testSourcesRoot) {
        this.testSourcesRoot = testSourcesRoot;
    }

    public double[] getOutput() {
        return output;
    }

    public void setOutput(double[] output) {
        this.output = output;
    }

    public List<AudioFileSpectrum> getAudioFilesSpectrum() {
        return audioFilesSpectrum;
    }

    public void setAudioFilesSpectrum(List<AudioFileSpectrum> audioFilesSpectrum) {
        this.audioFilesSpectrum = audioFilesSpectrum;
    }
}
