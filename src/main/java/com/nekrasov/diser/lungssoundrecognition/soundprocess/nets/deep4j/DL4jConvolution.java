package com.nekrasov.diser.lungssoundrecognition.soundprocess.nets.deep4j;

import com.nekrasov.diser.lungssoundrecognition.soundprocess.dataset.DataSetProvider;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.nets.NeuralNet;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.report.NetworkReport;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.ConvolutionLayer;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.conf.layers.SubsamplingLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.nd4j.linalg.schedule.MapSchedule;
import org.nd4j.linalg.schedule.ScheduleType;

import java.util.HashMap;
import java.util.Map;

public class DL4jConvolution implements NeuralNet {
    //    private static final Logger LOG = LoggerFactory.getLogger(DL4jConvolution.class);
    private static final int NUMBER_OF_EPOCHS = 50000;
    private final DataSetProvider dataSetProvider;
    private final String name;
    private final MultiLayerNetwork network;
    private final int outputSize;
    private final int width;
    private final int height;
    private int numberOfEpochs = NUMBER_OF_EPOCHS;
    private NetworkReport networkReport;


    private double learningRate;

    public DL4jConvolution(DataSetProvider dataSetProvider, String name, int width, int height, int outputSize) {
        this.dataSetProvider = dataSetProvider;
        this.name = name;
        this.outputSize = outputSize;
        this.width = width;
        this.height = height;
        network = buildNetwork();
        networkReport = new NetworkReport();
    }

    @Override
    public void train() {
        DataSet trainDataSet = (DataSet) dataSetProvider.createDataSet();
        networkReport.notchStartTime();
        for (int i = 0; i < numberOfEpochs; i++) {
            network.fit(trainDataSet);
//            LOG.info("Completed epoch {}", i);
        }
        networkReport.notchEndTime("Training");
    }

    public void test(DataSet dataSet) {
        // test the neural network
        System.out.println("Evaluate model....");

        INDArray predicted = network.output(dataSet.getFeatureMatrix());
        Evaluation eval = new Evaluation(outputSize);
        INDArray labels = dataSet.getLabels();
        networkReport.notchStartTime();
        eval.eval(labels, predicted);
        networkReport.notchEndTime("Testing");

        //Print the evaluation statistics
        System.out.println(eval.stats());
    }

    @Override
    public void test() {
        networkReport = new NetworkReport();
        DataSet testDataSet = (DataSet) dataSetProvider.createTestDataSet();
        test(testDataSet);
    }


    private MultiLayerNetwork buildNetwork() {
        int channels = 1;
        int seed = 1234;

        Map<Integer, Double> lrSchedule = new HashMap<>();
        lrSchedule.put(0, 0.06); // iteration #, learning rate
        lrSchedule.put(200, 0.05);
        lrSchedule.put(600, 0.028);
        lrSchedule.put(800, 0.0060);
        lrSchedule.put(1000, 0.001);

        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                .seed(seed)
                .l2(0.0005)
                .updater(new Nesterovs(new MapSchedule(ScheduleType.ITERATION, lrSchedule)))
                .weightInit(WeightInit.XAVIER)
                .list()
                .layer(0, new ConvolutionLayer.Builder(5, 5)
                        .nIn(channels)
                        .stride(1, 1)
                        .nOut(20)
                        .activation(Activation.IDENTITY)
                        .build())
                .layer(1, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX)
                        .kernelSize(2, 2)
                        .stride(2, 2)
                        .build())
//                .layer(2, new ConvolutionLayer.Builder(5, 5)
//                        .stride(1, 1) // nIn need not specified in later layers
//                        .nOut(50)
//                        .activation(Activation.IDENTITY)
//                        .build())
//                .layer(3, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX)
//                        .kernelSize(2, 2)
//                        .stride(2, 2)
//                        .build())
                .layer(2, new DenseLayer.Builder().activation(Activation.SIGMOID)
                        .nOut(50).build())
                .layer(3, new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
                        .nOut(outputSize)
                        .activation(Activation.SOFTMAX)
                        .build())
                .setInputType(InputType.convolutionalFlat(height, width, 1))
                .backprop(true).pretrain(false).build();

        MultiLayerNetwork net = new MultiLayerNetwork(conf);
        net.init();
        net.setListeners(new ScoreIterationListener(100));
//        LOG.debug("Total num of params: {}", net.numParams());
        return net;
    }

    @Override
    public void setLearningRate(double learningRate) {
        network.setLearningRate(learningRate);
        this.learningRate = learningRate;
    }

    @Override
    public void setBatchMode(boolean batchMode) {

    }

    @Override
    public void setMaxError(double maxError) {

    }

    @Override
    public void setMaxIterations(int maxIterations) {
        network.setEpochCount(maxIterations);
        numberOfEpochs = maxIterations;
    }

    @Override
    public double getTrainingTime() {
        return 0;
    }
}
