package com.nekrasov.diser.lungssoundrecognition.soundprocess.nets.neuroph;

/**
 * Created by on 1/29/2017.
 */

import com.nekrasov.diser.lungssoundrecognition.soundprocess.dataset.DataSetProvider;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.nets.NeuralNet;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.report.NetworkReport;
import com.nekrasov.diser.lungssoundrecognition.soundprocess.utils.NetworkUtils;
import org.neuroph.core.NeuralNetwork;
import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;
import org.neuroph.core.learning.LearningRule;
import org.neuroph.nnet.ConvolutionalNetwork;
import org.neuroph.nnet.learning.ConvolutionalBackpropagation;


/**
 * This is simple sample to show that learning procedure works
 */
public class ConvolutionNet implements NeuralNet {

    private ConvolutionalNetwork convolutionNet;
    private DataSetProvider<DataSet> dataSetProvider;
    private DataSet trainingSet;
    private String name;
    private LearningRule learningRule;
    private double trainingTime;
    private NetworkReport networkReport;


    public ConvolutionNet(DataSetProvider dataSetProvider, String name, int width, int height) {
        this.networkReport = new NetworkReport();
        this.dataSetProvider = dataSetProvider;
        this.name = name;
        trainingSet = (DataSet) dataSetProvider.createDataSet();
        convolutionNet = buildNetwork(dataSetProvider, width, height);
        learningRule = new ConvolutionalBackpropagation();
        learningRule.addListener(new BackPropagationListener());
        convolutionNet.setLearningRule((ConvolutionalBackpropagation) learningRule);
        System.out.println(convolutionNet.getLayers().get(1).getNeurons().get(0).getWeights().length);
    }

    private ConvolutionalNetwork buildNetwork(DataSetProvider dataSetProvider, int width, int height) {
        ConvolutionalNetwork convolutionNet = new ConvolutionalNetwork.Builder()
                .withInputLayer(width, height, 1)
                .withConvolutionLayer(width, height, 50)
//                .withPoolingLayer(2, 2)
//                .withFullConnectedLayer(3)
                .withFullConnectedLayer(dataSetProvider.getLungsSoundClasses().size())
                .build();
        return convolutionNet;
    }


    public void train() {
        NetworkUtils.printLayers(convolutionNet);
        // CREATE DATA SET


        // TRAIN NETWORK
        NetworkUtils.printLearningRuleName(convolutionNet);

//        LearnListener learnListener = new LearnListener(convolutionNet.getLearningRule());
//        learnListener.startTask();
        System.out.println("LR: " + convolutionNet.getLearningRule().getLearningRate());
        networkReport.notchStartTime();
        convolutionNet.learn(trainingSet);
//        convolutionNet.getLearningRule().addListener(new BackPropagationListener(200000));
        networkReport.notchEndTime("Training");

        System.out.println("Iterations: " + convolutionNet.getLearningRule().getCurrentIteration());
        System.out.println("Total error: " + convolutionNet.getLearningRule().getTotalNetworkError());
//        convolutionNet.save(name + ".nnet");
        System.out.println("Done training!");
//        test(trainingSet);
    }

    public void test(DataSet testSet) {
//        NeuralNetwork network = NeuralNetwork.createFromFile(name + ".nnet");
        networkReport.notchStartTime();
        for (DataSetRow testSetRow : testSet.getRows()) {
            convolutionNet.setInput(testSetRow.getInput());
            convolutionNet.calculate();
            networkReport.addRowToResult(convolutionNet.getOutput().clone(), testSetRow.getDesiredOutput().clone(),
                    testSetRow.getLabel());
        }
        networkReport.notchEndTime("Testing");
        networkReport.createAndPrintReport();
    }

    public void test() {
        networkReport = new NetworkReport();
        DataSet testSet = dataSetProvider.createTestDataSet();
        test(testSet);
        networkReport.dumpResult();
    }

    public void setLearningRate(double learningRate) {
        ((ConvolutionalBackpropagation) learningRule).setLearningRate(learningRate);

    }

    public void setBatchMode(boolean batchMode) {
        ((ConvolutionalBackpropagation) learningRule).setBatchMode(batchMode);

    }

    public void setMaxError(double maxError) {
        convolutionNet.getLearningRule().setMaxError(maxError);

    }

    public void setMaxIterations(int maxIterations) {
        ((ConvolutionalBackpropagation) learningRule).setMaxIterations(maxIterations);
    }

    public double getTrainingTime() {
        return trainingTime;
    }

}