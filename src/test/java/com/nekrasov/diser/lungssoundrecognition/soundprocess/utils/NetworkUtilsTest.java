package com.nekrasov.diser.lungssoundrecognition.soundprocess.utils;


import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Arrays;

public class NetworkUtilsTest {

    @Test
    public void testGetNormalizedDataUpperLowBorder() {
        double[] input = {1, 0, -3, 22, 333.2};
        double[] normalizedData = NetworkUtils.getNormalizedDataUpperLowBorder(input);

        double min = Arrays.stream(normalizedData).min().getAsDouble();
        double max = Arrays.stream(normalizedData).max().getAsDouble();
        Assert.assertEquals(min, 0.01d);
        Assert.assertEquals(max, 1d);
    }


    @Test
    public void testGetNormalizedData() {
        double[] input = {1, 0, -3, 22, 333.2};
        double[] normalizedData = NetworkUtils.getNormalizedData(input);

        double min = Arrays.stream(normalizedData).min().getAsDouble();
        double max = Arrays.stream(normalizedData).max().getAsDouble();
        Assert.assertEquals(min, 0d);
        Assert.assertEquals(max, 1d);
    }
}