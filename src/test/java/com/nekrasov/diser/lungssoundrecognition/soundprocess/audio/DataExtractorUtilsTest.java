package com.nekrasov.diser.lungssoundrecognition.soundprocess.audio;

import junit.framework.TestCase;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class DataExtractorUtilsTest extends TestCase {

    @Test
    public void testGetWindowsAmount() {
        Assert.assertEquals(DataExtractorUtils.getWindowsAmount(512, 246), 1);
        Assert.assertEquals(DataExtractorUtils.getWindowsAmount(512, 512), 1);
        Assert.assertEquals(DataExtractorUtils.getWindowsAmount(512, 1023), 1);
        Assert.assertEquals(DataExtractorUtils.getWindowsAmount(512, 1024), 2);
        Assert.assertEquals(DataExtractorUtils.getWindowsAmount(512, 1027), 2);
    }


    @Test
    public void testAvgSpectrum() {
        ArrayList<Float> spectrum = new ArrayList<>(
                Arrays.asList(1f, 1f, 1f, 1f, 1f,
                        1f, 1f, 1f, 1f, 1f,
                        1f, 1f, 1f, 1f, 1f));
        float[] etalon = new float[]{1f, 1f, 1f, 1f, 1f};
        float[] avgSpectrum = DataExtractorUtils.calculateAgvSpectrum(5, spectrum);
        assertEquals(Arrays.toString(etalon), Arrays.toString(avgSpectrum));

        spectrum = new ArrayList<>(
                Arrays.asList(1f, 1f, 1f, 1f, 1f,
                        1f, 1f, 1f, 1f, 1f,
                        1f, 1f, 1f, 1f, 1f,
                        5f, 5f));
        etalon = new float[]{2f, 2f, 1f, 1f, 1f};
        avgSpectrum = DataExtractorUtils.calculateAgvSpectrum(5, spectrum);
        assertEquals(Arrays.toString(etalon), Arrays.toString(avgSpectrum));


        spectrum = new ArrayList<>(
                Arrays.asList(1f, 1f, 1f));
        etalon = new float[]{1f, 1f, 1f, 0f, 0f};
        avgSpectrum = DataExtractorUtils.calculateAgvSpectrum(5, spectrum);
        assertEquals(Arrays.toString(etalon), Arrays.toString(avgSpectrum));
    }
}